package com.example.fresher2022_kotlin_hieunm68.kotlin.coroutines


import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

fun main() = runBlocking {
    val time = measureTimeMillis {
        repeat(100_000) { // launch 100_000 coroutines
            launch {
                print("hello ")
            }
        }
    }
    println(time)
}