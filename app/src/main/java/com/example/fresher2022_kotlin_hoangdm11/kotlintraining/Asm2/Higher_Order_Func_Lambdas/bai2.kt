package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm2.Higher_Order_Func_Lambdas

import java.util.*

fun findPrimeFromRange(start: Int, end: Int, checkPrime: (Int) -> Boolean):MutableList<Int> {
    val arr: MutableList<Int> = ArrayList()
    for(i in start..end) {
        if(checkPrime(i)) {
            arr.add(i)
        }
    }
    return arr
}

fun main() {
    print("Nhap vao so thu nhat: ")
    val start = Scanner(System.`in`).nextInt();
    print("Nhap vao so thu hai: ")
    val end = Scanner(System.`in`).nextInt();
    val res = findPrimeFromRange(start,end, :: checkPrime)
    print("So nguyen to trong khoang $start -> $end la: ")
    for(i in 0 until res.size) {
        print("${res[i]}, ")
    }
}