package com.example.kotlin_exam

import java.lang.Exception

class Stack<T>() {
    var elements: List<T> = emptyList()

    fun put(value: T) {

        print("Add $value to stack\n")
        elements += value
    }

    fun pop(): T? {
        return try {
            val temp = elements.get(elements.size - 1)
            elements -= temp
            temp
        } catch (e: Exception) {
            print("Can not pop\n")
            null
        }
    }

    fun peek(): T? {
        return try {
            elements[elements.size - 1]
        } catch (e: Exception) {
            print("Stack is empty\n")
            null
        }
    }

    fun isEmpty(): Boolean {
        return elements.isEmpty()
    }

    fun getSize(): Int {
        return elements.size
    }

    override fun toString(): String {
        if (this.isEmpty()) return "Stack is empty now\n"
        var res = ""
        var temp = elements.reversed()
        for (i in 0..elements.size - 1) {
            res += temp[i].toString()
            print("${temp[i]}\n")
        }
        return res
    }
}


fun main() {
    val s: Stack<Int> = Stack()
    print(s.toString())
    s.put(1)
    print("Peek: ${s.peek()}\n")
    print("Stack size: ${s.getSize()}\n")
    s.put(2)
    print("Peek: ${s.peek()}\n")
    s.put(3)
    print("Peek: ${s.peek()}\n")
    print("Stack size: ${s.getSize()}\n")
    print("Pop value ${s.pop()}\n")
    print("Peek: ${s.peek()}\n")
    print("Pop value ${s.pop()}\n")
    print("Peek: ${s.peek()}\n")
    print("Pop value ${s.pop()}\n")
    print("Pop value ${s.pop()}\n")
}