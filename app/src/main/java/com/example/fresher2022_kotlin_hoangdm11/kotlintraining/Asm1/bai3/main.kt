package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm1.bai3

import java.util.*

fun readArray():Array<Int> {
    print("Nhap do dai mang: ")
    val a = Scanner(System.`in`).nextInt();
    // create array has a value 0 ex: a = 4 -> array = {0,0,0,0}
    val array = Array(a) {0}

    for(i in 0 until a) {
        print("Nhap gia tri tai index $i: ")
        array[i] = Scanner(System.`in`).nextInt()
    }
    return  array;
}

fun main() {
    val array = readArray();
    print("Sau khi sap xep mang: ")
    array.sort()
    for(element in array) {
        print("$element")
    }

}