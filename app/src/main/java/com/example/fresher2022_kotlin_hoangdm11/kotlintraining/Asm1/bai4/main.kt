package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm1.bai4

fun wordCount(str: String): Int {
    val trimmedStr = str.trim()
    return if (trimmedStr.isEmpty()) {
        0
    } else {
        trimmedStr.split("\\s+".toRegex()).size
    }
}

fun formatString(str: String): String {
    val trimmedStr = str.trim();
    val array = trimmedStr.split(".")
    var res: String = ""
    for (i in array) {
        i.replaceFirstChar {
            it.uppercaseChar()
        }
     res += ".$i"
    }
    return  res
    }


fun main() {
    print("Nhap 1 chuoi: ")
    val input = readln();
    print("${formatString(input)} \n")
    print("So tu trong chuoi ${input.trim()}: ${wordCount(input)}")

}