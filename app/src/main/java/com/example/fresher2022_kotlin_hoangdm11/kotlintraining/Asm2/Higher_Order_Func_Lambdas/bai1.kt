package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm2.Higher_Order_Func_Lambdas

import java.util.*

fun checkPrime(n: Int):Boolean {
    var i: Int
    var m = 0
    var flag = 0
    m = n / 2
    if (n == 0 || n == 1) {
    } else {
        i = 2
        while (i <= m) {
            if (n % i == 0) {
                flag = 1
                break
            }
            i++
        }
        if (flag == 0) {
        }
    } //end of else
    return flag != 1
}

fun findPrimeToN(n: Int, checkPrime: (Int) -> Boolean):MutableList<Int> {
    val arr: MutableList<Int> = ArrayList()
    for(i in 2..n) {
        if(checkPrime(i)) {
            arr.add(i)
        }
    }
    return arr
}

fun findSumOfPrime(n: Int):MutableList<String>? {
    val arrayOfPrime = findPrimeToN(n, ::checkPrime)
    print("So nguyen to trong khoang tu 0 -> $n la: ")
    for(i in 0 until arrayOfPrime.size) {
        print("${arrayOfPrime[i]}, ")
    }
    var res:MutableList<String> = ArrayList()
    for(i in  0..arrayOfPrime.size-2) {
        for(j in i+1..arrayOfPrime.size-1) {
            if(arrayOfPrime[i]+arrayOfPrime[j] == n) {
                res.add("\n${arrayOfPrime[i]} + ${arrayOfPrime[j]} = $n")
            }
        }
    }
    return if( res.isEmpty() ) null
    else res
}

fun main() {
    print("Nhap vao 1 so nguyen: ")
    val input = Scanner(System.`in`).nextInt();
    val res:MutableList<String>? = findSumOfPrime(input)
    if(res == null) {
        print("Khong co bieu thuc nao thoa man")
    }
    else {
        for(i in 0 until res.size) {
            print("${res[i]}\n")
        }
    }
}