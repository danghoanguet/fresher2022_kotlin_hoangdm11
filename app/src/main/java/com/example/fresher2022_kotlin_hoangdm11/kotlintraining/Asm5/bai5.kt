package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm5

import java.util.*

class Dob(var day: Int = 0, var month: Int = 0, var year: Int = 0) {

    fun showDobInfo(): String {
        return ("${this.day}//${this.month}//${this.year}")
    }

    fun inputDobInfo() {
        var input: List<String>
        do {
            print("Nhap ngay sinh theo mau 'ngay/thang/nam': ")
            input = Scanner(System.`in`).next().split("/")
        } while (input.size != 3 || input[input.size-1].length != 4)
        day = input[0].toInt()
        month = input[1].toInt()
        year = input[2].toInt()
    }
}

class Employee(
    var name: String = "Unknown",
    var gender: String = "Unknown",
    var dob: Dob = Dob(),
    var phone: String = "Unknown",
) {
    var id: String = UUID.randomUUID().toString()
    var education: String = "Trung Cap"
    var hasInfomation: Boolean = false

    constructor(
        _name: String,
        _gender: String,
        _dob: Dob,
        _phone: String,
        _id: String,
        _education: String
    ) : this(
        _name,
        _gender,
        _dob,
        _phone,
    ) {
        this.id = _id
        this.education = _education

    }

    fun readInputInfo() {
        print("Nhap ten: ")
        this.name = readln()
        do {
            print("Nhap gioi tinh: ")
            this.gender = readln()
        } while (this.gender.trim().lowercase() != "nam" && this.gender.trim()
                .lowercase() != "nu"
        )
        this.dob.inputDobInfo()
        print("Nhap so dien thoai: ")
        this.phone = readln()
        do {
            print("Nhap trinh do chuyen mon: ")
            this.education = readln()
        } while (this.education.trim().lowercase() != "trung cap" && this.education.trim()
                .lowercase() != "cao dang" && this.education.trim().lowercase() != "dai hoc"
        )
        hasInfomation = true
    }

    fun showInfo() {
        print("Name: ${this.name}\nGender: ${this.gender}\nDob: ${this.dob.showDobInfo()}\nPhone: ${this.phone}\nId: ${this.id}\nEducation: ${this.education}\n")
    }

    fun management() {
        var input: String = ""
        do {
            print(
                "\nNhap chuc nang (add, display, update, find)" +
                        ", De ket thuc nhap 'end': "
            )
            input = readln().trim().lowercase()
            when (input) {
                "add" -> this.readInputInfo()
                "display" -> {
                    if (this.hasInfomation) this.showInfo()
                    else print("Not found information\n")
                }
                "update" -> {
                    if (this.hasInfomation) this.readInputInfo()
                    else print("Not found information\n")
                }
                "find" -> {
                    if(this.hasInfomation) {
                        var input:String
                        do {
                            print("Vui long nhap ten hoac ma so de tim kiem: ")
                            input = readln().trim().lowercase()
                        }while (input.trim().isEmpty())
                        when(input) {
                            this.name.lowercase() -> {
                                print("Tim thay nhan vien ten: ${this.name}\n")
                                this.showInfo()
                            }
                            this.id.lowercase() -> {
                                print("Tim thay nhan vien ma so: ${this.name}\n")

                                this.showInfo()
                            }
                            else -> {
                                print("Not found information for ${input}\n")
                            }
                        }
                    }
                    else print("Not found information\n")
                }
                else -> print("Khong hop le, vui long nhap lai\n")
            }

        } while (input != "end")
    }

}

fun main() {
    var e = Employee()
    e.management()
}
