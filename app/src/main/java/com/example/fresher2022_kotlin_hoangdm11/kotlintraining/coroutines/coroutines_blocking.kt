package com.example.fresher2022_kotlin_hieunm68.kotlin.coroutines

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking { // chạy một coroutine
        println("Hello")
        delay(5000)
    }
    println("World")
}