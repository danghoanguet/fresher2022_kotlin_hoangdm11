package com.example.kotlin_exam

import java.lang.Exception

class Queue<T>() {
    var elements: List<T> = emptyList()

    fun enqueue(value: T) {
        print("Enqueue $value\n")
        elements += value
    }

    fun dequeue(): T? {
        return try {
            val res = elements[0]
            elements -= res
            return res
        } catch (e: Exception) {
            print("Can not dequeue\n")
            null
        }
    }

    fun peek(): T? {
        return try {elements[elements.size - 1]} catch (e:Exception) {
            null
        }
    }

    fun isEmpty(): Boolean {
        return elements.isEmpty()
    }

    fun getSize(): Int {
        return elements.size
    }

    override fun toString(): String {
        if (this.isEmpty()) return "Queue is empty now\n"
        var res = ""
        for (i in 0..elements.size-1) {
            res += elements[i].toString()
            print("${elements[i]}\n")
        }
        return res
    }
}

fun main() {
    val queue: Queue<Int> = Queue()
    print(queue.toString())
    queue.enqueue(1)
    print("Peek: ${queue.peek()}\n")
    print("Queue size: ${queue.getSize()}\n")
    queue.enqueue(2)
    print("Peek: ${queue.peek()}\n")
    queue.enqueue(3)
    print("Peek: ${queue.peek()}\n")
    print("Queue size: ${queue.getSize()}\n")
    print("dequeue value ${queue.dequeue()}\n")
    print("Peek: ${queue.peek()}\n")
    print("dequeue value ${queue.dequeue()}\n")
    print("Peek: ${queue.peek()}\n")
    print("dequeue value ${queue.dequeue()}\n")
    print("Queue size: ${queue.getSize()}\n")
    print("dequeue value ${queue.dequeue()}\n")
}