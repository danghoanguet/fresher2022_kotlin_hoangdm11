package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm2.Higher_Order_Func_Lambdas

import java.util.*

fun sumNumber(n:Int, calculateSum: (Int) -> Int) {
  print(calculateSum(n))
}

fun main() {
    print("Nhap vao 1 so nguyen: ")
    val input = Scanner(System.`in`).nextInt();
    sumNumber(input) { n: Int ->
        var sum: Int = 0
        var temp = n
        while (temp > 0) {
            sum += temp % 10
            temp /= 10
        }
        sum
    }
}