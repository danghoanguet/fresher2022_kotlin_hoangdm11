package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm4

fun main() {
    val dummy1: MutableList<Int> = arrayListOf ( 1,2,3,4)
    val dummy2: MutableList<Double> = arrayListOf ( 1.0,2.0,3.0,4.0,5.0)

    print("Dummy1 after swap index 1 and 2: ${swap(dummy1,1,2)}\n")
    print("Dummy2 after swap index 0 and 1: ${swap(dummy2,0,1)}")

}
fun <T> swap(a: MutableList<T>, i: Int, j: Int):MutableList<T> {
    val t = a[i]
    a[i] = a[j]
    a[j] = t
    return a
}