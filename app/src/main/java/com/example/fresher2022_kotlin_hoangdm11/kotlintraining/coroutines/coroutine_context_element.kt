package com.example.fresher2022_kotlin_hieunm68.kotlin.coroutines

import kotlinx.coroutines.*

fun main() {
    // set context khi sử dụng runBlocking { } để start coroutine
    runBlocking(Dispatchers.IO + Job()) {
    }

// hoặc set context khi sử dụng launch { } để start coroutine
    GlobalScope.launch(newSingleThreadContext("demo_thread") + CoroutineName("demo_2") + NonCancellable) {

    }

}