package com.example.fresher2022_kotlin_hieunm68.kotlin.coroutines

import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

fun main() = runBlocking {
    val time = measureTimeMillis {
        val first = async(start = CoroutineStart.LAZY) { print1() }
        val second = async(start = CoroutineStart.LAZY) { print2() }
        first.start() // start the first one
        second.start() // start the second one
        println("The answer is ${first.await() + second.await()}")
    }
    println("Completed in $time ms")
}

suspend fun print1(): Int {
    delay(1000L)
    return 10
}

suspend fun print2(): Int {
    delay(1000L)
    return 20
}
