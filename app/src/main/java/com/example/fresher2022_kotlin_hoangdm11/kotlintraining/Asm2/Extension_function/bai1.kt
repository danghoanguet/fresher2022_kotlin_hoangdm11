package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm2.Extension_function

import java.util.*

fun Int.toHexString(): String {
    var decimal = this
    var rem = 0;
    var hex = ""
    val hexchars =
        charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')

    while (decimal > 0) {
        rem = decimal % 16;
        hex = hexchars[rem] + hex;
        decimal /= 16; }
    return hex
}

fun main() {
    print("Nhap vao 1 so nguyen: ")
    val input = Scanner(System.`in`).nextInt();
    print("So $input doi sang hex la: ${input.toHexString()}")
}

