package com.example.fresher2022_kotlin_hieunm68.kotlin.coroutines

fun functionA() { println("in ra A") }
fun functionB() { println("in ra B") }
fun main() {
    // chạy functionA và functionB
    functionA()
    functionB()
}