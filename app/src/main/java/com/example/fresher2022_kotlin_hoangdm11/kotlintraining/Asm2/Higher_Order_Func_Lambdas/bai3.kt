package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm2.Higher_Order_Func_Lambdas

import java.util.*

fun recurSum(n: Int): Int {
    return if (n <= 1) n else n + recurSum(n - 1)
}

fun findSum(): ((Int) -> Int) {
    return ::recurSum
}

fun main() {
    print("Nhap vao 1 so nguyen: ")
    val input = Scanner(System.`in`).nextInt()
    val sum = findSum()
    print("Tong: ${recurSum(input)}\n")
    print(sum(input))
}