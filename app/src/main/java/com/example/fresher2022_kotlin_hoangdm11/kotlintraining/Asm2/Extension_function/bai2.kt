package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm2.Extension_function

import java.util.*

fun String.hexToBinary(): String? {
    var hex = this
    var binary = ""

    // converting the accepted Hexadecimal
    // string to upper case
    hex = hex.uppercase(Locale.getDefault())

    var i: Int
    var ch: Char

    // loop to iterate through the length
    // of the Hexadecimal String
    i = 0
    while (i < hex.length) {
        // extracting each character
        ch = hex[i]

        // checking if the character is
        // present a HEX code
        when (ch) {
            '0' -> binary += "0000"
            '1' -> binary += "0001"
            '2' -> binary += "0010"
            '3' -> binary += "0011"
            '4' -> binary += "0100"
            '5' -> binary += "0101"
            '6' -> binary += "0110"
            '7' -> binary += "0111"
            '8' -> binary += "1000"
            '9' -> binary += "1001"
            'A' -> binary += "1010"
            'B' -> binary += "1011"
            'C' -> binary += "1100"
            'D' -> binary += "1101"
            'E' -> binary += "1110"
            'F' -> binary += "1111"
            else -> return "Invalid Hexadecimal String"
        }

        i++
    }
    // returning the converted Binary
    return binary
}

fun main() {
    print("Nhap vao 1 chuoi hex: ")
    val input = readLine();
    if (input != null) {
        print("So $input doi sang hex la: ${input.hexToBinary()}")
    }
}