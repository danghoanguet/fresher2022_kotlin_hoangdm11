package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm5

import java.util.*


fun inputArray(n:Int):MutableList<Int> {
    val a:MutableList<Int> = arrayListOf()
    var input:Int
    var i = 0
    try {
        while (i < n) {
            print("Nhap gia tri: ")
            input = Scanner(System.`in`).nextInt()
            if(input == 100)  throw InputMismatchException("Input can not be 100\n")
            a.add(i,input)
            i++
        }
    }catch (e: InputMismatchException) {
        print(e.message)
    }

    return a
}

fun main() {
    var a = inputArray(5)
    print(a)
}