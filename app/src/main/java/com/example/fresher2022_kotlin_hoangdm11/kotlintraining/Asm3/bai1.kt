package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm3

import java.util.*

open class Person(
    var name: String = "Person with no name",
    var gender: String = "Unknown"

) {
    var dob = "Unknown"
    var address = "Unknown"
        get() = field
        set(value) {
            field = value
        }

    init {
        print("a new person is being created...\n")
    }

    constructor(_name: String, _gender: String, _dob: String, _address: String) : this(
        _name,
        _gender
    ) {
        this.dob = _dob
        this.address = _address
    }

    open fun inputInfo() {
        print("Nhap ten: ")
        this.name = readln()
        print("Nhap gioi tinh: ")
        this.gender = readln()
        print("Nhap ngay thang nam sinh: ")
        this.dob = readln()
        print("Nhap dia chi: ")
        this.address = readln()

    }

    open fun showInfo() {
        print("name: ${this.name}\ngender: ${this.gender}\ndob: ${this.dob}\naddress: ${this.address}\n")
    }

}

class Student : Person {
    val Prefix_MSV = "SV"
    var msv: String = ""
    var dtb: Double = 0.0
    var email: String = ""
    var duocHocBong: Boolean = false

    constructor() : super() {
        print("a student is being created\n")

    }

    constructor(
        _name: String,
        _gender: String,
        _dob: String,
        _address: String,
        _msv: String,
        _dtb: Double,
        _email: String
    ) : super(_name, _gender, _dob, _address) {
        this.msv = Prefix_MSV + _msv
        this.dtb = _dtb
        this.email = _email
    }

    override fun inputInfo() {
        super.inputInfo()
        print("Nhap msv: ")
        this.msv = readln()
        print("Nhap diem trung binh: ")
        this.dtb = readln().toDouble()
        do {
            print("Nhap email: ")
            this.email = readln().trim()
        } while (
            !this.email.contains("@")
        )
    }

    override fun showInfo() {
        super.showInfo()
        setHocBong()
        print("Ma sinh vien: ${this.msv}\nDiem trung binh ${this.dtb}\nEmail: ${this.email}\nHoc bong: ${this.duocHocBong}")
    }

    fun setHocBong() {
        if (this.dtb >= 8.0) this.duocHocBong = true
    }
}

class Teacher : Person {
    var classRoom: String = ""
    var ratePerHour: Double = 0.0
    var hourPerMonth: Double = 0.0

    val classPrefix = arrayOf("G", "H", "I", "K", "L", "M")

    constructor() : super() {
        print("a teacher is being created\n")
    }

    constructor(
        _name: String,
        _gender: String,
        _dob: String,
        _address: String,
        _classRoom: String,
        _ratePerHour: Double,
        _hourPerMonth: Double
    ) : super(_name, _gender, _dob, _address) {
        this.classRoom = _classRoom
        this.ratePerHour = _ratePerHour
        this.hourPerMonth = _hourPerMonth
    }

    override fun inputInfo() {
        super.inputInfo()
        var temp: Boolean = false
        do {
            print("Nhap lop hoc: ")
            this.classRoom = readln().trim()
                .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
            for (element in classPrefix) {
                if (this.classRoom[0].toString() == element) {
                    temp = true
                }
            }
        } while (
            !temp
        )
        print("Nhap so gio day: ")
        this.hourPerMonth = readln().toDouble()
        print("Nhap luong day mot gio: ")
        this.ratePerHour = readln().toDouble()
    }

    override fun showInfo() {
        super.showInfo()
        print("Lop hoc: ${this.classRoom}\nLuong day mot gio: ${this.ratePerHour}\nSo gio day: ${this.hourPerMonth}\nLuong thang nay: ${this.calculateRatePerMonth()}")

    }

    fun calculateRatePerMonth(): Double {
        return when (this.classRoom[0].toString()) {
            "G", "H", "I", "K" -> this.ratePerHour * this.hourPerMonth
            else -> this.ratePerHour * this.hourPerMonth + 500000
        }

    }
}

fun main() {
    var student = Student()
    student.inputInfo()
    student.showInfo()
//    var teacher:Teacher = Teacher()
//    teacher.inputInfo()
//    teacher.showInfo()
}
