package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm5

import java.lang.Exception
import kotlin.collections.ArrayList


fun main() {
    val color:MutableList<String> = arrayListOf("a", "bcd", "asdas")
    val color2:MutableList<String> = arrayListOf("a", "bcd", "asdas", "as131", " sadasd")

    val list: MutableList<String> = ArrayList(color)
    val list2: MutableList<String> = ArrayList(color2)

    //add all element in list2 to list
    list.addAll(list2)
    //clear list2
    list2.clear()

    // uppercase all element in list
    print("List1: $list\nList2: $list2\n")
    for ( i in 0 until list.size) {
        list[i] = list[i].uppercase()
    }
    print("List1: $list\nList2: $list2\n")

    //remove element 4th to 6th in list
    for(i in 4..6) {
        try {
            list.removeAt(i)
        }
        catch (e: Exception) {
            print("Some thing wrongs\n")
        }
    }
    print("List1: $list\nList2: $list2\n")

    //dao nguoc list
    list.reverse()
    print("List1: $list\nList2: $list2\n")

}