package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm5

fun getElementAt(arr:MutableList<String>,index:Int):String? {
    try {
        return arr[index-1]
    }
    catch (e: IndexOutOfBoundsException) {
        print("Can not get element at index $index\n")
    }
    return null
}

fun main() {
    val color:MutableList<String> = arrayListOf("a", "bcd", "asdas","123", "123123")
    print(getElementAt(color,7))
}