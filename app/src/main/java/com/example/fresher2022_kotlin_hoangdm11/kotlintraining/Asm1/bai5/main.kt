package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm1.bai5

import java.util.*

fun main() {
    val month: Int
    val year: Int
    print("Nhập tháng:")
    month = Scanner(System.`in`).nextInt()
    print("Nhập năm:")
    year = Scanner(System.`in`).nextInt()
    var isLeapYear = false;
    if(year % 400 == 0 || year % 4 == 0) {
        isLeapYear = true;
    }
    var numberOfDayInMonth = 0;

    numberOfDayInMonth = if(month == 2 && isLeapYear) 28
    else 29;
    when(month) {
        1->numberOfDayInMonth = 31
        3->numberOfDayInMonth = 31
        4->numberOfDayInMonth = 30
        5->numberOfDayInMonth = 31
        6->numberOfDayInMonth = 30
        7->numberOfDayInMonth = 31
        8->numberOfDayInMonth = 31
        9->numberOfDayInMonth = 30
        10->numberOfDayInMonth = 31
        11->numberOfDayInMonth = 30
        12->numberOfDayInMonth = 31
    }
    print("Số ngày trong tháng: $numberOfDayInMonth");


//    if(month == 1) {
//        numberOfDayInMonth = 31
//    }
//    else if (month == 2 && isLeapYear) {
//        numberOfDayInMonth = 28
//    }
//    else if(month == 2 && !isLeapYear) {
//        numberOfDayInMonth = 29
//    }
//    if(month == 3) {
//        numberOfDayInMonth = 31
//    }
//    if(month == 4) {
//        numberOfDayInMonth = 30
//    }
//    if(month == 5) {
//        numberOfDayInMonth = 31
//    }
//    if(month == 6) {
//        numberOfDayInMonth = 30
//    }
//    if(month == 7) {
//        numberOfDayInMonth = 31
//    }
//    if(month == 8) {
//        numberOfDayInMonth = 31
//    }
//    if(month == 9) {
//        numberOfDayInMonth = 30
//    }
//    if(month == 10) {
//        numberOfDayInMonth = 31
//    }
//    if(month == 11) {
//        numberOfDayInMonth = 30
//    }
//    if(month == 12) {
//        numberOfDayInMonth = 31
//    }
}