package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm4


fun <T> findSizeArray(array: MutableList<T>):Int {
    return array.size
}

fun main() {
    val dummy1: MutableList<Int> = arrayListOf ( 1,2,3,4)
    val dummy2: MutableList<Double> = arrayListOf ( 1.0,2.0,3.0,4.0,5.0)

    print("Dummy1 size: ${findSizeArray(dummy1)}\n" )
    print("Dummy2 size: ${findSizeArray(dummy2)}" )

}