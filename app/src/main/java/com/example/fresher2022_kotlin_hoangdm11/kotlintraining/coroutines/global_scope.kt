package com.example.fresher2022_kotlin_hieunm68.kotlin.coroutines

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runBlocking<Unit> {
    val request = launch {
        // it spawns two other jobs, one with GlobalScope
        GlobalScope.launch {
            println("job1: GlobalScope and execute independently!")
            delay(10000)
            println("job1: I am not affected by cancellation")  // line code 1 này vẫn được in ra mặc dù bị delay 1000ms
        }
        // and the other inherits the parent context
        launch {
            delay(1000)
            println("job2: I am a child of the request coroutine")
            delay(10000)
            println("job2: I will not execute this line if my parent request is cancelled")
        }
    }
    delay(5000)
    request.cancel() // cancel processing of the request
    delay(10000) // delay a second to see what happens
    println("main: Who has survived request cancellation?")
}
