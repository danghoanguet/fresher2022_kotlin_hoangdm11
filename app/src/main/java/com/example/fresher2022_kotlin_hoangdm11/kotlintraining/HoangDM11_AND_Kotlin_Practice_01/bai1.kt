package com.example.kotlin_exam


fun palindrome(inputString: String):Boolean {
    var tempString = inputString.trim()
    var reverseString = ""
    var i = tempString.length-1
    while (i >= 0) {
        reverseString += tempString[i]
        if (tempString == reverseString) {

            return true
        }
        i--
    }
    return false
}
fun main() {
    print("${palindrome("abba")}\n")
    print("${palindrome("  abba ")}\n")
    print("${palindrome("abcdefg")}\n")
    print("${palindrome("abba")}\n")
    print("${palindrome("abb a")}\n")

}
