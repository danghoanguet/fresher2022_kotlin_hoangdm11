package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm4


fun <T : Comparable<T>> findMaxInRange(a: MutableList<T>, start: Int, end: Int): T {
    var max = a[0]
    for (element in start until end) {
        when (a[element]) {
            is Int, is Double, is Float -> {
                if (a[element] > max) max = a[element]
            }
            is String -> {
                if (a[element].toString().length > max.toString().length) {
                    max = a[element]
                }
            }
            else -> {
                if (a[element] > max) max = a[element]

            }
        }
    }
    return max
}

fun main() {
    val dummyInt: MutableList<Int> = arrayListOf(10, 2, 6, 4)
    print("Max int: ${findMaxInRange(dummyInt, 0, 4)}\n")
    val dummyDouble: MutableList<Double> = arrayListOf(1.0, 8.0, 3.0, 4.0)
    print("Max double: ${findMaxInRange(dummyDouble, 0, 4)}\n")
    val dummyString: MutableList<String> = arrayListOf("hello", "helo", "hello123", "helloo1231")
    print("Max string length: ${findMaxInRange(dummyString, 0, 4)}\n")
}