package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm5

import java.lang.Exception
import java.util.*

fun input():Double? {
    var input:Double? = null
    try {
        print("Nhap vao mot so thuc:  ")
        input = Scanner(System.`in`).nextDouble()

    } catch (
        e: Exception
    ) {
        print(e.toString())
        print("\n")
    }
    return input
}

fun divide(n1: Double?, n2: Double?):Double? {
    var res: Double?= 0.0
    try {
        if (n1 != null && n2 != null) {
            res = n1/ n2
        }
    }catch (e:Exception) {
        print(e.toString())
    }
    return res
}

fun main() {
    val number1 = input()?: return
    val number2 = input()?: return
    print("Number 1: $number1\nNumber2: $number2\n")
    print("$number1 divide $number2 = ${divide(number1,number2)}")
}