package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm1.bai2

fun readUserInput(): Int {
    var input: String?
    do {
        print("Nhap 1 so nguyen co 2 chu so: ")
        input = readln();
    } while (input == "" || (input is String && (input.toInt() !in 10..99)) || input == null );
    return input.toInt();
}

fun main() {
    val input = readUserInput()
    val binary = Integer.toBinaryString(input)
    val hex = Integer.toHexString(input)
    print("So nhi phan sau khi chuyen: $binary \n")
    print("So thap luc phan sau khi chuyen: $hex \n")
}