package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm3

abstract class Employee() {
    var name:String = "No name"
    var dob: String = ""
    var rate: Double = 0.0

    constructor(_name:String, _dob: String, _rate: Double) : this() {
        this.name = _name
        this.dob = _dob
        this.rate = _rate
    }

    init {
        print("A new employee is being created...\n")
    }

    abstract fun calculateRatePerMonth():Double
}

class ProducerEmployee: Employee {
    var products: Int = 0

    init {
        print("A producer is being created...\n")

    }

    constructor() : super() {
    }

    constructor(_name:String,_dob:String,_rate: Double,_products:Int) : super(_name,_dob,_rate) {
        this.products = _products
    }

    override fun calculateRatePerMonth():Double {
        return this.rate + products * 5000
    }
}


class OfficeEmployee: Employee {
    var daysOfWork: Int = 0

    init {
        print("A officer is being created...\n")

    }

    constructor() : super() {
    }

    constructor(_name:String,_dob:String, _daysOfWork:Int) : super(_name,_dob,0.0) {
        this.daysOfWork = _daysOfWork
    }

    override fun calculateRatePerMonth():Double {
        return daysOfWork.toDouble() * 500000
    }
}

fun main() {
    var producer1 = ProducerEmployee()
    print("${producer1.calculateRatePerMonth()}  \n")
    var producer2 = ProducerEmployee("HoangDM11", "8", 200.0,20)
    print("${producer2.calculateRatePerMonth()}  \n")

    var officer1 = OfficeEmployee()
    print("${officer1.calculateRatePerMonth()}  \n")
    var officer2 = OfficeEmployee("HoangDM11", "8", 2)
    print("${officer2.calculateRatePerMonth()}  \n")
}