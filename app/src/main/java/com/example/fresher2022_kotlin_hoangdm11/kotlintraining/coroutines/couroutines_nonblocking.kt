package com.example.fresher2022_kotlin_hieunm68.kotlin.coroutines

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


suspend fun sayHello() {
    delay(1000L) //ham delay k blocking thread nhu ham Thread.sleep
    println("Hello!")
}

fun main() {
    GlobalScope.launch { // chạy một coroutine trên background thread
        delay(10000L) // non-blocking coroutine bị delay 10s
        println("World,") // print từ World ra sau khi hết delay
    }
    println("Hello,") // main thread vẫn tiếp tục chạy xuống dòng code này trong khi coroutine vẫn đang bị delay 10s
    Thread.sleep(20000L) // block main thread 2s
    println("Kotlin")
}