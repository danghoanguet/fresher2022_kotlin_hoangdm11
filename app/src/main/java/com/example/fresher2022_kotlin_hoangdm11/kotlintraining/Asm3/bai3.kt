package com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm3

import java.util.ArrayList


abstract class Customer() {
    var type = "None"
    var year = 0
    var order = 0
    val paymentPerOrder = 100

    init {
        print("A new customer is being created...\n")
    }

    constructor(type: String, numberOfYears: Int, numberOfOrders: Int) : this() {
        this.type = type
        this.year = numberOfYears
        this.order = numberOfOrders
    }

    abstract fun sumPayment(): Double
}

class CustomerA : Customer {
    override fun sumPayment(): Double {
        return 1.1 * (this.order * this.paymentPerOrder)
    }

    init {
        print("A new customerA is being created...\n")
    }

    constructor() : super() {
        this.type = "A"
    }

    constructor( _numberOfYears: Int, _numberOfOrders: Int) : super(
        "A",
        _numberOfYears,
        _numberOfOrders
    )
}

class CustomerB : Customer {
    var discount: Double = 0.0
    override fun sumPayment(): Double {
        if (this.year * 0.05 > 0.5) this.discount = this.year * 0.05
        else this.discount = 0.5
        return (this.order * this.paymentPerOrder) * (1 - this.discount) * 1.1
    }

    constructor() : super() {
        this.type = "B"
    }

    constructor(
        _numberOfYears: Int,
        _numberOfOrders: Int,
        _discount: Double
    ) : super("B", _numberOfYears, _numberOfOrders)

    init {
        print("A new customerB is being created...\n")
    }
}

class CustomerC : Customer {
    override fun sumPayment(): Double {
        return 1.1 * (this.order * this.paymentPerOrder) * 0.5
    }

    constructor() : super() {
        this.type = "C"
    }

    constructor(_numberOfYears: Int, _numberOfOrders: Int) : super(
        "C",
        _numberOfYears,
        _numberOfOrders
    )

    init {
        print("A new customerC is being created...\n")
    }
}

class CompanyXYZ {
    var customerA: MutableList<Customer> = ArrayList()
    var customerB: MutableList<Customer> = ArrayList()
    var customerC: MutableList<Customer> = ArrayList()

    var totalSum = 0.0

    fun addNewCustomer(customer: Customer) {
        when (customer.type) {
            "A" -> customerA.add(customer)
            "B" -> customerB.add(customer)
            "C" -> customerC.add(customer)
        }
    }

    fun sumPaymentAListCustomer(listCustomer: MutableList<Customer>): Double {
        var sum = 0.0
        var i = 0
        for (customer in listCustomer) {
            print("Khach hang ${++i} da thanh toan: ${customer.sumPayment()}\n")
            sum += customer.sumPayment()
        }
        totalSum += sum
        return sum
    }

    fun sumPaymentAllCustomer(): Double {
        return this.totalSum
    }

    fun showInfo() {
        print("So khach hang loai A la: ${this.customerA.size}\n")
        print("So tien thu duoc tu khach hang loai A la: ${this.sumPaymentAListCustomer(this.customerA)}\n")

        print("So khach hang loai B la: ${this.customerB.size}\n")
        print("So tien thu duoc tu khach hang loai B la: ${this.sumPaymentAListCustomer(this.customerB)}\n")

        print("So khach hang loai C la: ${this.customerC.size}\n")
        print("So tien thu duoc tu khach hang loai C la: ${this.sumPaymentAListCustomer(this.customerC)}\n")

        print("Tong so tien thu duoc la: ${this.sumPaymentAllCustomer()}\n")
    }

}

fun main() {
       val dummyListCustomer:MutableList<Customer> = arrayListOf(
        CustomerA( 1, 10),
        CustomerA( 2, 20),
        CustomerA( 3, 30),

        CustomerB( 1, 10, 10.0),
        CustomerB( 2, 20, 20.0),
        CustomerB(3, 30, 30.0),

        CustomerC( 10, 100),
        CustomerC( 20, 200),
        CustomerC( 30, 300),)

        var companyXYZ: CompanyXYZ = CompanyXYZ()
        for(customer in dummyListCustomer) {
            print("Adding customer ${customer.type}\n")
            companyXYZ.addNewCustomer(customer)
        }
        companyXYZ.showInfo()

}