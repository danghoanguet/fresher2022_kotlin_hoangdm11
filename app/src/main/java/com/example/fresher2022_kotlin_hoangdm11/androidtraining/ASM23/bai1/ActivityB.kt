package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM23.bai1

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2.Product

class ActivityB : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b)

        val b = this.intent.extras
        val myList = b?.getStringArrayList("myList")
        Log.i("my list", myList.toString())

        val product = getIntent().getSerializableExtra("product") as Product
    }
}