package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai1.ConstraintLayoutDemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.example.fresher2022_kotlin_hoangdm11.R

class CL_ActivityB : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cl_b)
        title = "CL_ActivityB"
        val data = this.intent.extras

        findViewById<TextView>(R.id.textView2).text = ("Chào mừng " + data!!.getString("HO_TEN")) ?: ""
        findViewById<TextView>(R.id.textView3).text = ("Tài khoản: " + data.getString("TAI_KHOAN"))?: ""
        findViewById<TextView>(R.id.textView5).text = ("Tuổi: "  + data.getString("TUOI"))?:""
        findViewById<TextView>(R.id.textView4).text = ("Giới tính: "+ data.getString("GIOI_TINH"))?:""

        val btnOK = findViewById<Button>(R.id.id1_buttonOK).setOnClickListener() {
            setResult(1)
            finish()
        }
        val btnCancel = findViewById<Button>(R.id.id1_buttonCancel).setOnClickListener() {
            setResult(2)
            finish()
        }

        Log.i("","Tai khoan: ${data?.getString("TAI_KHOAN")}\nHo ten: ${data?.getString("HO_TEN")}\nTuoi: ${data?.getString("TUOI")}\nGioi tinh: ${data?.getString("GIOI_TINH")}\n")

    }
}