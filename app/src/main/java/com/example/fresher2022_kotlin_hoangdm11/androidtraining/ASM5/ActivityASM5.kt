package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM5

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2.Product
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM7.FormFragment

class ActivityASM5 : AppCompatActivity(),FragmentTransactionInterface {
    val fragmentManager = supportFragmentManager
    val fragmentTransaction = fragmentManager.beginTransaction()
    val formFragment = FormFragment(this)
    val detailsFragment = DetailsFragment(this)
    val recyclerViewFragment = RecyclerViewFragment(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asm5)

        //Add fragment
        //3 Dòng tiếp theo để add detailsFragment vào layout sau đó put lại vào backStack
        //nếu ko có đoạn này thì khi replace formFargment cho detailsFragment
        // thì hàm onCreateView sẽ không được gọi -> ko có view // - Không cần nữa
        // vì khi replace mình tạo mới 1 detailsFragment luôn -> chưa tối ưu
        //TODO: Improvement on replace fragment
        fragmentTransaction.add(detailsFragment,"Detail")
       // fragmentTransaction.remove(detailsFragment)
        //fragmentTransaction.addToBackStack("Detail")
        fragmentTransaction.add(R.id.frame_layout_top,formFragment,"Form")
        fragmentTransaction.add(R.id.frame_layout_bottom,recyclerViewFragment,"RecyclerView")
        fragmentTransaction.commit()
    }

    override fun send1(intent: Intent) {
        print("send1 call in main\n")
        val productName = intent.extras!!.get("name") as String
        val productPrice = intent.extras!!.get("price").toString().toFloat()
        val productDescribe = intent.extras!!.get("describe").toString()
        val productId = intent.extras!!.get("id").toString().toInt()
        val product = Product(productId,productName,productPrice,productDescribe,"")
        var bundle = Bundle()
        bundle.putString("name",productName)
        bundle.putString("id",productId.toString())
        bundle.putString("describe",productDescribe)
        bundle.putString("price",productPrice.toString())

        // Không set trực tiếp được ?
//        findViewById<TextView>(R.id.detailFragment_textView_tenSP).text = "Tên sản phẩm: $productName"
//        findViewById<TextView>(R.id.detailFragment_textView_giaSP).text = "Giá sản phẩm: $productPrice"
//        findViewById<TextView>(R.id.detailFragment_textView_motaSP).text = "Mô tả sản phẩm: $productDescribe"
//        findViewById<Button>(R.id.detailFragment_button_capnhat).setOnClickListener() {
//            var intent = Intent()
//            var bundle = Bundle()
//            bundle.putString("name",productName)
//            bundle.putString("id",productId.toString())
//            bundle.putString("describe",productDescribe)
//            bundle.putString("price",productPrice.toString())
//            intent.putExtras(bundle)
//            setResult(1,intent)
//            finish()
//        }
//        findViewById<ImageView>(R.id.detailFragment_image_view).setImageResource(R.drawable.image_2)

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
       // val detailsFragment = DetailsFragment()
        // TODO: Improvement on replace fragment
        val detailsFragment =  DetailsFragment(this)
        detailsFragment.arguments = bundle
        //detailsFragment.fillForm(product)
        fragmentTransaction.replace(R.id.frame_layout_top,detailsFragment)
        //Không cần addToBackStack vì FormFragment đã từng được đưa lên View
        //fragmentTransaction.addToBackStack("Form")
        fragmentTransaction.commit()

    }

    override fun send2(intent: Intent) {
        print("send2 call in main\n")
        val productName = intent.extras!!.get("name") as String
        val productPrice = intent.extras!!.get("price").toString().toFloat()
        val productDescribe = intent.extras!!.get("describe").toString()
        val productId = intent.extras!!.get("id").toString().toInt()
        val product = Product(productId,productName,productPrice,productDescribe,"")

        var bundle = Bundle()
        bundle.putString("name",productName)
        bundle.putString("id",productId.toString())
        bundle.putString("describe",productDescribe)
        bundle.putString("price",productPrice.toString())
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        //TODO: Improvement on replace fragment
        val formFragment = FormFragment(this)
        formFragment.arguments = bundle
        //formFragment.fillForm(product)
        fragmentTransaction.replace(R.id.frame_layout_top,formFragment)
        fragmentTransaction.commit()    }

    override fun send3(intent:Intent) {
        val i = intent.extras
        val product: Product = Product(i!!.getString("id")!!.toInt(),i.getString("name")!!,i.getString("price")!!.toFloat(),i.getString("describe")!!,"")
        print(product!!.productID)
        recyclerViewFragment.modifieProductList(product)
    }
}

