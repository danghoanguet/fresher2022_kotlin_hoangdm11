package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM5

import android.content.Intent

interface FragmentTransactionInterface {
    fun send1(intent: Intent)
    fun send2(intent: Intent)
    fun send3(intent: Intent)
}