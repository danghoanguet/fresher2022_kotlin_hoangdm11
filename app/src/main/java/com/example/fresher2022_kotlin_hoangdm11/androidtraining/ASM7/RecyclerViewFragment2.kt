package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM7

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2.ASM4_ActivityB
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2.Product
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2.RecylcerViewAdapterProduct

class RecyclerViewFragment2
    : Fragment() {

    // TODO: Fix issue ở ASM7 bằng cách sửa listProduct thành static varible
    var listProduct: ArrayList<Product> = arrayListOf(
        Product(
            1,
            "San pham 1",
            1.0F,
            "describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1",
            "drawable-v24/image_1.jpeg"
        ),
        Product(
            2, "San pham 2",
            2.0F, "describe 2", "drawable-v24/image_2.jpeg"
        ),
        Product(
            3, "San pham 3",
            3.0F, "describe 3", "drawable-v24/image_3.jpeg"
        ),
        Product(
            4, "San pham 4",
            4.0F, "describe 4", "drawable-v24/image_4.jpeg"
        ),
    )
    val productRecyclerViewAdapter = RecylcerViewAdapterProduct(listProduct)
    var recylerViewProduct: RecyclerView? = null
    var product: Product? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("RecyclerView","onCreateView called\n")

        val view = inflater.inflate(R.layout.recycler_view_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("RecyclerView","onViewCreated called\n")
        product = Product(
            arguments?.getString("id")!!.toInt(),
            arguments?.getString("name")!!,
            arguments?.getString("price")!!.toFloat(),
            arguments?.getString("describe")!!,
            "drawable-v24/image_3.jpeg"
        )
        val linearLayoutManager = LinearLayoutManager(context)
        recylerViewProduct =
            view.findViewById<RecyclerView>(R.id.recycler_view_fragment_listproduct)
        (recylerViewProduct as RecyclerView?)!!.adapter = productRecyclerViewAdapter
        (recylerViewProduct as RecyclerView?)!!.layoutManager = linearLayoutManager

        productRecyclerViewAdapter.setOnItemClickListener(object :
            RecylcerViewAdapterProduct.onItemClickListener {
            override fun onItemClick(position: Int) {
                print("onItemClick call in RecyclerViewFragment\n")
                val mProduct = (productRecyclerViewAdapter.getItem(position) as Product?)!!
                var bundle = Bundle()
                bundle.putString("name", mProduct.name)
                bundle.putString("id", mProduct.productID.toString())
                bundle.putString("describe", mProduct.describe)
                bundle.putString("price", mProduct.price.toString())
                bundle.putString("image", mProduct.imageURL)

//                Navigation.findNavController(view).navigate(R.id.detailsFragment,bundle)
                val navController = NavHostFragment.findNavController(this@RecyclerViewFragment2)
                navController.navigate(R.id.detailsFragment, bundle)

            }
        })
        modifieProductList(product!!)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun modifieProductList(product: Product) {
        if (checkId(product.productID, listProduct) >= 0) {
            listProduct[checkId(product.productID, listProduct)] = product
            // listViewProduct.smoothScrollToPosition(checkId(productId, listProduct))
            recylerViewProduct!!.smoothScrollToPosition(checkId(product.productID, listProduct))
            productRecyclerViewAdapter.notifyItemChanged(checkId(product.productID, listProduct))
            Toast.makeText(context, "Cập nhật sản phẩm thành công", Toast.LENGTH_SHORT).show()

        } else {
            listProduct.add(0, product)
            productRecyclerViewAdapter.notifyItemInserted(0)
            recylerViewProduct!!.smoothScrollToPosition(0)
            Toast.makeText(context, "Thêm sản phẩm thành công", Toast.LENGTH_SHORT).show()
        }
    }

    fun checkId(productId: Int, listProduct: ArrayList<Product>): Int {
        for (i in 0 until listProduct.size) {
            if (listProduct[i].productID == productId) {
                return i
            }
        }
        return -1
    }

}