package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM8

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM8.SongAdapter.onSongClickListener

class Activity8 : AppCompatActivity(),MediaController{

    private val listSong: ArrayList<SongInfo> = arrayListOf(
        SongInfo("Bai 1", R.raw.mysong),
        SongInfo("Bai 2", R.raw.mysong),
        SongInfo("Bai 3", R.raw.mysong),
        SongInfo("Bai 4", R.raw.mysong),
        SongInfo("Bai 1", R.raw.mysong),
        SongInfo("Bai 2", R.raw.mysong),
        SongInfo("Bai 3", R.raw.mysong),
        SongInfo("Bai 4", R.raw.mysong),
        SongInfo("Bai 1", R.raw.mysong),
        SongInfo("Bai 2", R.raw.mysong),
        SongInfo("Bai 3", R.raw.mysong),
        SongInfo("Bai 4", R.raw.mysong),
        SongInfo("Bai 1", R.raw.mysong),
        SongInfo("Bai 2", R.raw.mysong),
        SongInfo("Bai 3", R.raw.mysong),
        SongInfo("Bai 4", R.raw.mysong),
        SongInfo("Bai 1", R.raw.mysong),
        SongInfo("Bai 2", R.raw.mysong),
        SongInfo("Bai 3", R.raw.mysong),
        SongInfo("Bai 4", R.raw.mysong),
    )
    var ACTION_PAUSE = 1
    var ACTION_RESUME = 2
    var ACTION_PREV = 3
    var ACTION_NEXT = 4
    var ACTION_JUMP = 5

    private var mSong: SongInfo? = null
    private var isPlaying: Boolean = false
    private var btn_play: Button? = null

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            var bundle = p1!!.extras
            if (bundle != null) {
                Log.d("OnActivity8", "\nReceive broadcast\n")

                mSong = bundle.getSerializable("song") as SongInfo
                isPlaying = bundle.getBoolean("status")
                var action: Int = bundle.getInt("action")
                handleLayoutMusic(action)
            } else return
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_8)

        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter("send_data_to_activity"))

        val songAdapter = SongAdapter(listSong)
        val linearLayoutManager = LinearLayoutManager(this)
        val songView = findViewById<RecyclerView>(R.id.listsong)

        songView.adapter = songAdapter
        songView.layoutManager = linearLayoutManager

        songAdapter.setOnItemClickListener(object : onSongClickListener {
            override fun onItemClick(position: Int) {
                Log.d("OnActivity8", "Clicked Song ${listSong[position].title}\n")
                var bundle = Bundle()
                bundle.putSerializable("song", listSong[position])
                val intent = Intent(this@Activity8, MyForegroundService::class.java)
                intent.putExtras(bundle)

                startForegroundService(intent)
            }
        })

    }

    private fun handleLayoutMusic(action: Int) {
        when (action) {
            0 -> {
                findViewById<FrameLayout>(R.id.ll_bot_8).visibility = View.VISIBLE
                var bundle = Bundle()
                bundle.putSerializable("song", mSong)
                val fragmentManager = supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()
                val playingFragment = PlayingFragment(this)
                playingFragment.arguments = bundle
                fragmentTransaction.add(R.id.ll_bot_8, playingFragment)
                fragmentTransaction.commit()
            }
            ACTION_PAUSE -> {
                Log.d("OnActivity8", "PAUSE SONG")
            }
            ACTION_RESUME -> {
                Log.d("OnActivity8", "RESUME SONG")
            }
            ACTION_JUMP -> {
                Log.d("OnActivity8", "SKIP SONG")
            }
        }
    }

    private fun sendActionToService(action: Int) {
        var intent = Intent("send_data_to_service")
        var bundle = Bundle()
        bundle.putSerializable("song", mSong)
        bundle.putBoolean("status", isPlaying)
        bundle.putInt("action", action)
        intent.putExtras(bundle)

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }


override fun onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    super.onDestroy()
}

    override fun onClickPlayOrPause() {
        if(isPlaying) {
            sendActionToService(ACTION_PAUSE)
        } else {
            sendActionToService(ACTION_RESUME)
        }
    }

    override fun onClickPrevSong() {
        sendActionToService(ACTION_PREV)
    }

    override fun onClickNextSong() {
        sendActionToService(ACTION_NEXT)
    }

    override fun onClickSeekBar() {
        sendActionToService(ACTION_JUMP)
    }

}