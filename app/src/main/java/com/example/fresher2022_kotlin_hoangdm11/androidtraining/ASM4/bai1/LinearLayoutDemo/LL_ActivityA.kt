package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai1.ConstraintLayoutDemo.LinearLayoutDemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai1.RelativeLayoutDemo.RL_ActivityB

class LL_ActivityA : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ll_a)
        title = "LL_ActivityA"

        val taiKhoan = findViewById<EditText>(R.id.id_edit_text_account).text
        val hoTen = findViewById<EditText>(R.id.id_edit_text_name).text
        val tuoi = findViewById<EditText>(R.id.id_edit_text_age).text
        val dropdown = findViewById<Spinner>(R.id.id_spinner)
        var gioiTinh = ""
        val items = arrayOf("Nam", "Nữ", "Khác")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)
        dropdown.adapter = adapter
        dropdown.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                gioiTinh = parent.getItemAtPosition(pos).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        val btn = findViewById<Button>(R.id.id_button_confirm)
        btn.setOnClickListener() {
            val bundle = Bundle()
            bundle.putString("TAI_KHOAN",taiKhoan.toString())
            bundle.putString("HO_TEN",hoTen.toString())
            bundle.putString("TUOI",tuoi.toString())
            bundle.putString("GIOI_TINH",gioiTinh)

            val intent = Intent(this, LL_ActivityB::class.java)
            intent.putExtras(bundle)
            startActivityForResult(intent,1)

            Log.i("","Tai khoan: $taiKhoan\nHo ten: $hoTen\nTuoi: $tuoi\nGioi tinh: $gioiTinh\n")
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1) {
            if (resultCode == 1) {
                //Clear form
                findViewById<EditText>(com.example.fresher2022_kotlin_hoangdm11.R.id.id1_et_taikhoan).text.clear()
                findViewById<EditText>(com.example.fresher2022_kotlin_hoangdm11.R.id.id1_et_hoten).text.clear()
                findViewById<EditText>(com.example.fresher2022_kotlin_hoangdm11.R.id.id1_et_tuoi).text.clear()
                findViewById<Spinner>(com.example.fresher2022_kotlin_hoangdm11.R.id.id_spinner1).setSelection(0)
            }
            else if( resultCode == 2) {
            }
        } else {
            print("requestCode mismatch")
        }
    }
    }
