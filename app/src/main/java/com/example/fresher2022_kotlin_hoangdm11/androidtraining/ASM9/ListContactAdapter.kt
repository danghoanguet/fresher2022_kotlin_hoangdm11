package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM9

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.fresher2022_kotlin_hoangdm11.R

class ListContactAdapter(var listContact: ArrayList<Contact>):
    RecyclerView.Adapter<ListContactAdapter.ContactViewHolder>() {

    inner class ContactViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        var tvNumber: TextView? = null
        var tvName: TextView? = null
        init {
            tvNumber = itemView.findViewById(R.id.contact_number) as TextView
            tvName = itemView.findViewById(R.id.contact_name) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val contactView: View = inflater.inflate(R.layout.contacts_list_item,parent,false)
        return ContactViewHolder(contactView)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact:Contact = listContact[position]
        holder.tvName!!.text = contact.name
        holder.tvNumber!!.text = contact.number
    }

    override fun getItemCount(): Int {
        return listContact.size
    }
}


class Contact(var name:String, var number: String) {}