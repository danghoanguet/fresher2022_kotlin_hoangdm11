package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM8

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.fresher2022_kotlin_hoangdm11.R

class SongAdapter(var listSong: ArrayList<SongInfo>) :
    RecyclerView.Adapter<SongAdapter.SongViewHolder>() {

    interface onSongClickListener {
        fun onItemClick(position: Int)
    }
    inner class SongViewHolder(itemView: View, listener: onSongClickListener) :
        RecyclerView.ViewHolder(itemView) {
        var tvSongName: TextView? = null
        var btnPlay: Button? = null

        init {
            tvSongName = itemView.findViewById(R.id.tv_songName_8) as TextView
            btnPlay = itemView.findViewById(R.id.btn_playsong_8) as Button

            itemView.findViewById<Button>(R.id.btn_playsong_8).setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }
    }


    fun setOnItemClickListener(listener: onSongClickListener) {
        mListener = listener
    }

    private lateinit var mListener: onSongClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val songView: View = inflater.inflate(R.layout.song_view, parent, false)
        return SongViewHolder(songView, mListener)
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val song: SongInfo = listSong[position]
        holder.tvSongName!!.text = String.format("%s", song.title)
    }

    override fun getItemCount(): Int {
        return listSong.size
    }

}