package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM6.Bai2

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.example.fresher2022_kotlin_hoangdm11.R

class PreferenceFragment: PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preference_screen_demo)
    }
}