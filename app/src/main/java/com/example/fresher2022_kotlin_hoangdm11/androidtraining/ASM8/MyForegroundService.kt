package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM8

import android.annotation.SuppressLint
import android.app.*
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.app.PendingIntent.FLAG_MUTABLE
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.fresher2022_kotlin_hoangdm11.MainActivity
import com.example.fresher2022_kotlin_hoangdm11.R

class MyForegroundService : Service() {
    private val CHANNEL_ID = "ForegroundServiceChannel"
    private var mSong: SongInfo? = null
    private var mediaPlayer: MediaPlayer? = null
    private var isPlaying: Boolean = false
    var ACTION_PAUSE = 1
    var ACTION_RESUME = 2
    var ACTION_PREV = 3
    var ACTION_NEXT = 4
    var ACTION_JUMP = 5

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            var bundle = p1!!.extras
            if (bundle != null) {
                Log.d("MyForegroundService", "\nReceive broadcast\n")

                mSong = bundle.getSerializable("song") as SongInfo
                isPlaying = bundle.getBoolean("status")
                var action: Int = bundle.getInt("action")
                handleActionMusic(action)
            } else return
        }
    }

    override fun onCreate() {
        Log.d(
            "ForegroundService",
            "onCreate: "
        )
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter("send_data_to_service"))

        super.onCreate()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        var bundle = intent.extras
        if (bundle != null) {
            mSong = bundle.getSerializable("song") as SongInfo
            Log.d("MyForegroundService", "song name ${mSong!!.title}")

            //create notification
            createNotificationChannel()

            //send notification
            sendNotification(mSong!!)

            //start music
            if (mediaPlayer == null) {
                Log.d("MyForegroundService", "\nPlay music")
                mediaPlayer = MediaPlayer.create(this, mSong!!.res)
            }
            mediaPlayer!!.start()
            isPlaying = true
            sendActionToActivity(0)
        }
        return START_NOT_STICKY
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            serviceChannel.setSound(null, null)

            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
    }

    private fun sendNotification(song: SongInfo) {
        val notificationIntent = Intent(this, Activity8::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent,
            FLAG_IMMUTABLE
        )
        val notification = Notification.Builder(this, CHANNEL_ID).setContentTitle("Playing music")
            .setContentText("Content example").setContentIntent(pendingIntent).build()

        startForeground(1, notification)
    }

    private fun handleActionMusic(action: Int) {
        when (action) {
            ACTION_PAUSE -> {
                if (mediaPlayer != null && isPlaying) {
                    mediaPlayer!!.pause()
                    isPlaying = false
                    sendActionToActivity(ACTION_PAUSE)
                    Toast.makeText(this, "PAUSE SERVICE", Toast.LENGTH_SHORT).show()
                }
            }
            ACTION_RESUME -> {
                if (mediaPlayer != null && !isPlaying) {
                    mediaPlayer!!.start()
                    isPlaying = true
                    sendActionToActivity(ACTION_RESUME)
                    Toast.makeText(this, "RESUME SERVICE", Toast.LENGTH_SHORT).show()
                }
            }
            ACTION_NEXT -> {
                Toast.makeText(this, "NEXTSONG SERVICE", Toast.LENGTH_SHORT).show()
                sendActionToActivity(ACTION_NEXT)

            }
            ACTION_PREV -> {
                Toast.makeText(this, "PREVIOUS SONG SERVICE", Toast.LENGTH_SHORT).show()
                sendActionToActivity(ACTION_PREV)

            }
            ACTION_JUMP -> {
                Toast.makeText(this, "JUMP SEEKBAR SERVICE", Toast.LENGTH_SHORT).show()
                sendActionToActivity(ACTION_PREV)
            }

        }
    }

    private fun sendActionToActivity(action: Int) {
        var intent = Intent("send_data_to_activity")
        var bundle = Bundle()
        bundle.putSerializable("song", mSong)
        bundle.putBoolean("status", isPlaying)
        bundle.putInt("action", action)
        intent.putExtras(bundle)

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onDestroy() {
        if (mediaPlayer != null) {
            mediaPlayer!!.release()
            mediaPlayer = null
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        super.onDestroy()
    }

    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

}