package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM10

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.fresher2022_kotlin_hoangdm11.R

class FragmentChia5Observer(var temp:UpdateView): Observer {
    var number = 0

    override fun update(number: Int) {
        this.number = number
        temp.updateViewChia5(number)
    }
}
class FragmentChia5(var fragmentChia5Observer: FragmentChia5Observer,var mOnFinish: OnFinish ) : Fragment(){
    var tv_number:TextView? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        print("FragmentChia5 onCreateView call \n")
        val view = inflater.inflate(R.layout.fragment_chia5,container,false)
        tv_number = view.findViewById<TextView>(R.id.tv_number_chia5)
        updateView(fragmentChia5Observer.number)
        return view
    }

    fun updateView(number: Int) {
        object : Thread() {
            override fun run() {
                var i = 0
                while (i <= number) {
                    try {
                        if(i % 5 == 0 && i != 0) {
                            if (i % 2 == 0 && i != 0) {
                                tv_number!!.setBackgroundResource(R.color.solid_red)
                            } else {
                                tv_number!!.setBackgroundResource(R.color.solid_blue)
                            }
                            activity?.runOnUiThread(Runnable { tv_number?.text = i.toString() })
                        }
                        sleep(500)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    i++
                }
                tv_number!!.text = ""
                tv_number!!.setBackgroundResource(R.color.solid_gray)
                mOnFinish.onFinish()
            }
        }.start()
    }
}