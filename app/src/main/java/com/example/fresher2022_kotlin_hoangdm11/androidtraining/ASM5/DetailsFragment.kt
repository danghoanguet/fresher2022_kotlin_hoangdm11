package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM5

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2.Product

class DetailsFragment(var mFragmentTransactionInterface: FragmentTransactionInterface): Fragment(){
    var textViewTenSP: TextView? = null
    var textViewGiaSP: TextView? = null
    var textViewMotaSP: TextView? = null
    var imageView: ImageView? = null
    var textViewButton: Button? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        print("DetailsFragment onCreateView call \n")
        val view = inflater.inflate(R.layout.detail_fragment,container,false)
        textViewTenSP = view.findViewById<TextView>(R.id.detailFragment_textView_tenSP)
        textViewGiaSP = view.findViewById<TextView>(R.id.detailFragment_textView_giaSP)
        textViewMotaSP= view.findViewById<TextView>(R.id.detailFragment_textView_motaSP)
        imageView = view.findViewById<ImageView>(R.id.detailFragment_image_view)
        textViewButton= view.findViewById<Button>(R.id.detailFragment_button_capnhat)
        arguments?.let {
            print("DetailsFragment arg call\n")
            var intent = Intent()
            var bundle = Bundle()
            var productName= it.getString("name")
            var productId=it.getString("id")
            var productDescribe=it.getString("describe")
            var productPrice=it.getString("price")
            bundle.putString("name",productName)
            bundle.putString("id",productId)
            bundle.putString("describe",productDescribe)
            bundle.putString("price",productPrice)
            intent.putExtras(bundle)
            textViewTenSP!!.text = "Tên sản phẩm: ${productName}"
            textViewGiaSP!!.text = "Giá sản phẩm: ${productPrice}"
            textViewMotaSP!!.text = "Mô tả sản phẩm: $productDescribe}"
            imageView!!.setImageResource(R.drawable.image_1)
            textViewButton!!.setOnClickListener() {
                mFragmentTransactionInterface.send2(intent)
            }
        }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
//        arguments?.let {
//            textViewTenSP!!.text = it.get("name").toString()
//            textViewGiaSP!!.text = it.get("price").toString()
//            textViewMotaSP!!.text = it.get("describe").toString()
//            imageView!!.setImageResource(R.drawable.image_5)
//
//        }
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun fillForm(product: Product) {
        textViewTenSP!!.text = "Tên sản phẩm: ${product.name}"
        textViewGiaSP!!.text = "Giá sản phẩm: ${product.productID}"
        textViewMotaSP!!.text = "Mô tả sản phẩm: ${product.describe}"
        imageView!!.setImageResource(R.drawable.image_1)
        textViewButton!!.setOnClickListener() {
            var intent = Intent()
            var bundle = Bundle()
            bundle.putString("name",product.name)
            bundle.putString("id",product.productID.toString())
            bundle.putString("describe",product.describe)
            bundle.putString("price", product.price.toString())
            intent.putExtras(bundle)
            mFragmentTransactionInterface.send2(intent)
        }

//            setResult(1,intent)
//            finish()
//        }
    }
}