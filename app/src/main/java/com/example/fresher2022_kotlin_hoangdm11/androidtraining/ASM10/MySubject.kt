package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM10

import android.util.Log
import android.widget.Toast
import java.util.*
import kotlin.collections.ArrayList


interface Observer {
    fun update(number: Int)
}

internal interface Subject {
    fun registerObserver(o: Observer)
    fun unregisterObserver(o: Observer)
    fun notifyObservers()
}

class MySubject : Subject {
    private var observerList: ArrayList<Observer> = ArrayList()
    private var number = 0
    var isPlaying = false

    fun playOrStop(maxNumber: Int) {
        this.sendNumber(maxNumber)
    }

    fun sendNumber(number: Int) {
        print("is playing2: ${isPlaying}\n")
        isPlaying = number > 0
        this.number = number
        notifyObservers()
    }

    override fun registerObserver(o: Observer) {
        observerList.add(o)
    }

    override fun unregisterObserver(o: Observer) {
        observerList.remove(o)
    }

    override fun notifyObservers() {
        val it: Iterator<Observer> = observerList.iterator()
        while (it.hasNext()) {
            val o = it.next()
            o.update(number)
        }
    }

}