package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM7

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2.Product
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM5.FragmentTransactionInterface

class FormFragment2 : Fragment() {
    var editTextTenSP: EditText? = null
    var editTextGiaSP: EditText? = null
    var editTextMotaSP: EditText? = null
    var editTextMaSP: EditText? = null
    var formButton: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        print("formFragment onCreateView called\n")
        val view = inflater.inflate(R.layout.form_fragment, container, false)
        editTextTenSP = view.findViewById<EditText>(R.id.formFragmentedit_text_tenSP)
        editTextGiaSP = view.findViewById<EditText>(R.id.formFragmentedit_text_giaSP)
        editTextMotaSP = view.findViewById<EditText>(R.id.formFragmentedit_text_moTaSP)
        editTextMaSP = view.findViewById<EditText>(R.id.formFragmentedit_text_maSP)
        formButton = view.findViewById<Button>(R.id.formFragmentbutton_xacNhan)
        formButton!!.setOnClickListener() {
            if (editTextTenSP!!.text.isNotEmpty() && editTextGiaSP!!.text.isNotEmpty() && editTextMotaSP!!.text.isNotEmpty() && editTextMaSP!!.text.isNotEmpty()) {
                var bundle = Bundle()
                bundle.putString("name", editTextTenSP!!.text.toString())
                bundle.putString("id", editTextMaSP!!.text.toString())
                bundle.putString("describe", editTextMotaSP!!.text.toString())
                bundle.putString("price", editTextGiaSP!!.text.toString().toFloat().toString())

                //Navigation.findNavController(view).navigate(R.id.recyclerViewFragment,bundle)

                val navController = NavHostFragment.findNavController(this)
                navController.navigate(R.id.recyclerViewFragment, bundle)
            }
            print("FormButton Clicked!!\n")
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.let {
            print("formFragment arg call\n")
            editTextTenSP!!.text = Editable.Factory.getInstance().newEditable(it.getString("name"))
            editTextGiaSP!!.text = Editable.Factory.getInstance().newEditable(it.getString("price"))
            editTextMotaSP!!.text =
                Editable.Factory.getInstance().newEditable(it.getString("describe"))
            editTextMaSP!!.text = Editable.Factory.getInstance().newEditable(it.getString("id"))
        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun fillForm(product: Product) {
        print("fill form called \n")
        editTextTenSP!!.setText(product.name)
        editTextGiaSP!!.setText("${product.productID}")
        editTextMotaSP!!.setText(product.describe)
        formButton!!.setOnClickListener() {
            print("Tag: Clicked\n")
            var intent = Intent()
            var bundle = Bundle()
            bundle.putString("name", product.name)
            bundle.putString("id", product.productID.toString())
            bundle.putString("describe", product.describe)
            bundle.putString("price", product.price.toString())
            intent.putExtras(bundle)
//            setResult(1,intent)
//            finish()
        }
    }
}