package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.example.fresher2022_kotlin_hoangdm11.R

class ASM4_ActivityB : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asm4_b)

        val intent = this.intent.extras
        val productName = intent!!.get("name") as String
        val productPrice = intent.get("price").toString()
        val productDescribe = intent.get("describe").toString()
        val productId = intent.get("id").toString()


        findViewById<TextView>(R.id.id_textView_tenSP).text = "Tên sản phẩm: $productName"
        findViewById<TextView>(R.id.id_textView_giaSP).text = "Giá sản phẩm: $productPrice"
        findViewById<TextView>(R.id.id_textView_motaSP).text = "Mô tả sản phẩm: $productDescribe"
        findViewById<Button>(R.id.button_capnhat).setOnClickListener() {
            var intent = Intent()
            var bundle = Bundle()
            bundle.putString("name",productName)
            bundle.putString("id",productId)
            bundle.putString("describe",productDescribe)
            bundle.putString("price",productPrice)
            intent.putExtras(bundle)
            setResult(1,intent)
            finish()
        }
        findViewById<ImageView>(R.id.image_view).setImageResource(R.drawable.image_1)
    }
}