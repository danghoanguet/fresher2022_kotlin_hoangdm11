package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM6.Bai1

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.fresher2022_kotlin_hoangdm11.R

class MyDialogFragment(var mDialog_interface : MyDialogInterface): DialogFragment() {
    private var tvNo: TextView? = null
    private var tvYes: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         var view = inflater.inflate(R.layout.my_dialog_fragment, container)
        view.findViewById<TextView>(R.id.tv_yes).setOnClickListener() {
            mDialog_interface.sendData("You click Yes")
            dismiss()
        }
        view.findViewById<TextView>(R.id.tv_no).setOnClickListener() {
            mDialog_interface.sendData("You click No")
            dismiss()
        }
        return view
    }

    //Uncomment this to create Default dialog
//    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//
//        return AlertDialog.Builder(activity).setTitle("Confirm Dialog Title").setMessage("Confirm Dialog Content").setPositiveButton("YES",
//            DialogInterface.OnClickListener{_,_ -> mDialog_interface.sendData("You click Yes")}
//            ).setNegativeButton("NO",
//            DialogInterface.OnClickListener{_,_ -> mDialog_interface.sendData("You click No")}
//        ).create()
//    }


}