package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM23.bai4

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast

class MyReceiver2: BroadcastReceiver() {

    val ACTION_SEND = ".androidtraining.bai4.ACTION_SEND"
    val KEY_NUM = "NUMBER"

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i("","Im on onReceive")
        if (intent != null) {
            when(intent.action) {
                ACTION_SEND -> {
                    val data = intent.extras
                    val number = data?.getInt(KEY_NUM)
                    Log.i("","Receiver1: $number")
                    Toast.makeText(context, "Receiver2 : " + (number!!), Toast.LENGTH_SHORT ).show()
                    if(number == 2){
                        Log.i("","Finish!")
                        abortBroadcast()
                    }
                }
                else -> {
                    Log.i("Receiver: ","Can not find action")
                }
            }
        } else {
            Log.i("Receiver: ", "intent null ")
        }
    }

}