package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM10

import android.graphics.Color.red
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.kotlintraining.Asm2.Higher_Order_Func_Lambdas.checkPrime
import java.lang.Math.sqrt

class FragmentNguyenToObserver(var temp:UpdateView): Observer {
    var number = 0
    private fun checkPrime(n: Int): Boolean {

        // Number less than or equal 1 is not prime
        if(n <= 1)
            return false

        // 2 is only even prime number
        if(n == 2)
            return true

        // Check if number (> 2) is even. If yes, it is not prime.
        if(n % 2 == 0)
            return false

        var flag = true

        val root  = sqrt(n.toDouble()).toInt()

        // We need to check till square root of n only to find whether n is prime.
        for(i in 3..root step 2) {
            if((n % i) == 0) {
                flag = false
                break
            }
        }

        return flag
    }

    override fun update(number: Int) {
        this.number = number
        temp.updateViewNguyenTo(number)
    }
}
class FragmentNguyenTo(var fragmentNguyenToObserver: FragmentNguyenToObserver, var mOnFinish: OnFinish ) : Fragment(){
    var tv_number:TextView? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        print("FragmentNguyenTo onCreateView call \n")
        val view = inflater.inflate(R.layout.fragment_nguyento,container,false)
        tv_number = view.findViewById<TextView>(R.id.tv_number_nguyento)
        updateView(fragmentNguyenToObserver.number)
        return view
    }

    fun updateView(number: Int) {
        object : Thread() {
            override fun run() {
                var i = 0
                while (i <= number) {
                    try {
                        if(i % 3 == 0 && i != 0) {
                            if (checkPrime(i)) {
                                tv_number!!.setBackgroundResource(R.color.solid_red)
                            } else {
                                tv_number!!.setBackgroundResource(R.color.solid_blue)
                            }
                            activity?.runOnUiThread(Runnable { tv_number?.text = i.toString() })
                        }
                        sleep(500)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    i++
                }
                tv_number!!.text = ""
                tv_number!!.setBackgroundResource(R.color.solid_gray)
                mOnFinish.onFinish()
            }
        }.start()
    }

}