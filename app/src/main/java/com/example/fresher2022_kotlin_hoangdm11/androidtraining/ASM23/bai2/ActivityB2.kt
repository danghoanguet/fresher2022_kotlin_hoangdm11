package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM23.bai2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.example.fresher2022_kotlin_hoangdm11.R

class ActivityB2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b2)
        Log.i("info", "Im on ActivityB2")


        val b = this.intent.extras
        val firstNumber = b?.getString("first_number")
        val secondNumber = b?.getString("second_number")
        var sum = 0
        try {
            sum = (firstNumber!!.toInt() + secondNumber!!.toInt())

        } catch (e: NumberFormatException) {
            //Exception for input ""
            print(e.message)
        }
        print("first number: $firstNumber \n second number: $secondNumber\n")
        val btn = findViewById<Button>(R.id.button2)
        btn.setOnClickListener {
            val data = Intent()
            data.putExtra("sum", sum)
            setResult(3, data)
            finish()
        }
    }
}