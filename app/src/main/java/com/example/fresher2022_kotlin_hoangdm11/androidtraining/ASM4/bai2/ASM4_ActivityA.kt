package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.MotionEvent
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import com.example.fresher2022_kotlin_hoangdm11.R


class ASM4_ActivityA : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asm4_a)

        var listProduct: ArrayList<Product> = arrayListOf(
            Product(
                1,
                "San pham 1",
                1.0F,
                "describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1describe 1",
                "drawable-v24/image_1.jpeg"
            ),
            Product(
                2, "San pham 2",
                2.0F, "describe 2", "drawable-v24/image_2.jpeg"
            ),
            Product(
                3, "San pham 3",
                3.0F, "describe 3", "drawable-v24/image_3.jpeg"
            ),
            Product(
                4, "San pham 4",
                4.0F, "describe 4", "drawable-v24/image_4.jpeg"
            ),
        )
        var editText_maSP = findViewById<EditText>(R.id.id_edit_text_maSP)
        var editText_tenSP = findViewById<EditText>(R.id.id_edit_text_tenSP)
        var editText_giaSP = findViewById<EditText>(R.id.id_edit_text_giaSP)
        var editText_motaSP = findViewById<EditText>(R.id.id_edit_text_moTaSP)

        //ListView
//        var productListViewAdapter: ListAdapterProduct = ListAdapterProduct(listProduct)
//        var listViewProduct: ListView = findViewById(R.id.listproduct)
//        listViewProduct.adapter = productListViewAdapter
//
//        listViewProduct.onItemClickListener =
//            AdapterView.OnItemClickListener { parent, view, position, id ->
//                val product = productListViewAdapter.getItem(position) as Product?
//
//                var bundle = Bundle()
//                bundle.putString("name", product!!.name)
//                bundle.putInt("id", product.productID)
//                bundle.putString("describe", product.describe)
//                bundle.putFloat("price", product.price)
//                bundle.putString("image", product.imageURL)
//
//                var intent = Intent(this, ASM4_ActivityB::class.java)
//                intent.putExtras(bundle)
//                startActivityForResult(intent, 1)
//            }

//        RecyclerView
        val productRecyclerViewAdapter = RecylcerViewAdapterProduct(listProduct)
        val linearLayoutManager = LinearLayoutManager(this)
        val recylerViewProduct = findViewById<RecyclerView>(R.id.listproduct)
        recylerViewProduct.adapter = productRecyclerViewAdapter
        recylerViewProduct.layoutManager = linearLayoutManager

        productRecyclerViewAdapter.setOnItemClickListener(object :
            RecylcerViewAdapterProduct.onItemClickListener {
            override fun onItemClick(position: Int) {
                val product = productRecyclerViewAdapter.getItem(position) as Product?
                var bundle = Bundle()
                bundle.putString("name", product!!.name)
                bundle.putInt("id", product.productID)
                bundle.putString("describe", product.describe)
                bundle.putFloat("price", product.price)
                bundle.putString("image", product.imageURL)

                var intent = Intent(this@ASM4_ActivityA, ASM4_ActivityB::class.java)
                intent.putExtras(bundle)
                startActivityForResult(intent, 1)
            }

        })


        findViewById<Button>(R.id.id_button_xacNhan).setOnClickListener() {
            if (editText_maSP.text.isNotEmpty() && editText_tenSP.text.isNotEmpty() && editText_giaSP.text.isNotEmpty() && editText_motaSP.text.isNotEmpty()
            ) {
                val productId =
                    editText_maSP.text.toString().toInt()
                val productName = editText_tenSP.text.toString()
                var productPrice =
                    editText_giaSP.text.toString().toFloat()
                val productDescribe =
                    editText_motaSP.text.toString()
                val product = Product(
                    productId,
                    productName,
                    productPrice,
                    productDescribe,
                    "drawable-v24/image_3.jpeg"
                )
                if (checkId(productId, listProduct) >= 0) {
                    listProduct[checkId(productId, listProduct)] = product
                    // listViewProduct.smoothScrollToPosition(checkId(productId, listProduct))
                    recylerViewProduct.smoothScrollToPosition(checkId(productId, listProduct))
                    productRecyclerViewAdapter.notifyItemChanged(checkId(productId, listProduct))
                    Toast.makeText(this, "Cập nhật sản phẩm thành công", Toast.LENGTH_SHORT).show()

                } else {
                    listProduct.add(0, product)
                    productRecyclerViewAdapter.notifyItemInserted(0)
                    recylerViewProduct.smoothScrollToPosition(0)
                    Toast.makeText(this, "Thêm sản phẩm thành công", Toast.LENGTH_SHORT).show()

                }

                clearForm()
//                productListViewAdapter.notifyDataSetChanged()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == 1) {
                //Clear form
                findViewById<EditText>(R.id.id_edit_text_maSP).text =
                    Editable.Factory.getInstance().newEditable(data!!.extras!!.getString("id"))
                findViewById<EditText>(R.id.id_edit_text_tenSP).text =
                    Editable.Factory.getInstance().newEditable(data.extras!!.getString("name"))
                findViewById<EditText>(R.id.id_edit_text_giaSP).text =
                    Editable.Factory.getInstance().newEditable(data.extras!!.getString("price"))
                findViewById<EditText>(R.id.id_edit_text_moTaSP).text =
                    Editable.Factory.getInstance().newEditable(data.extras!!.getString("describe"))
            } else if (resultCode == 2) {
            }
        } else {
            print("requestCode mismatch")
        }
    }

    fun clearForm() {
        findViewById<EditText>(R.id.id_edit_text_maSP).text.clear()
        findViewById<EditText>(R.id.id_edit_text_tenSP).text.clear()
        findViewById<EditText>(R.id.id_edit_text_giaSP).text.clear()
        findViewById<EditText>(R.id.id_edit_text_moTaSP).text.clear()
    }

    fun checkId(productId: Int, listProduct: ArrayList<Product>): Int {
        for (i in 0 until listProduct.size) {
            if (listProduct[i].productID == productId) {
                return i
            }
        }
        return -1
    }
}