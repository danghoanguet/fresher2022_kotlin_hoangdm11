package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM23.bai3

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.fresher2022_kotlin_hoangdm11.R
import java.lang.Exception

class ActivityA3 : AppCompatActivity() {

    private val ACTION_ADD = ".androidtraining.bai3.ActivityA3.ACTION_ADD"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a3)
        Log.i("", "Im on ActivityA3")

        initView()
    }

    private fun initView() {
        val broadcastBtn = findViewById<Button>(R.id.button_broadcast)
        broadcastBtn.setOnClickListener() {
           onClickBroadcast()
        }
        val intentBtn = findViewById<Button>(R.id.button_intent)
        intentBtn.setOnClickListener() {
            onClickIntent()
        }
    }

    private fun onClickIntent() {
        Log.i("","Clicked intent")
        try {
            val intent2 = packageManager.getLaunchIntentForPackage("com.example.applicationb")
            startActivity(intent2)
        }catch (e: Exception) {
            print(e.toString())
        }

        Log.i("Intent","Sent Intent")
    }

    private fun onClickBroadcast() {
        Log.i("","Clicked broadcast")
        val intent = Intent()
        intent.action = ACTION_ADD
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
        intent.component = ComponentName("com.example.applicationb","com.example.applicationb.Receiver")
        sendBroadcast(intent)
        Log.i("Broadcast","Sent Broadcast")
    }
}



