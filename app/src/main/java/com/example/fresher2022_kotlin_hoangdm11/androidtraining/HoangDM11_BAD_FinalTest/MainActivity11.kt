package com.example.fresher2022_kotlin_hoangdm11.androidtraining.HoangDM11_BAD_FinalTest

import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.fresher2022_kotlin_hoangdm11.R


class MainActivity11 : AppCompatActivity() {
    var COUNTING_SERVICE_STATUS_OK = "OK"
    var COUNTING_SERVICE_STATUS_DESTROY = "DESTROY"
    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            var bundle = p1!!.extras
            if (bundle != null) {
                val sum = bundle.getLong("sum")
                val status = bundle.getString("status")
                try {
                    handleResponseService(sum,status)

                }catch (e:Exception) {
                    print(e.message)
                }
            } else return
        }
    }

    private fun handleResponseService(sum: Long, status: String?) {
        if(status == COUNTING_SERVICE_STATUS_OK) {
            Toast.makeText(this, "Counting Service done: Sum = $sum", Toast.LENGTH_LONG).show()
        }else if(status == COUNTING_SERVICE_STATUS_DESTROY){
            Toast.makeText(this, "Counting Service destroyed", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main11)
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter("send_response_to_activity"))

        findViewById<Button>(R.id.start_service).setOnClickListener() {
            val intent = Intent(this, CountingService::class.java)
            if(isMyServiceRunning(CountingService::class.java)) {
                Toast.makeText(this, "Counting Service is running", Toast.LENGTH_LONG).show()
            }else {
                Toast.makeText(this, "Start Counting Service", Toast.LENGTH_LONG).show()
                startService(intent)
            }
        }
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}