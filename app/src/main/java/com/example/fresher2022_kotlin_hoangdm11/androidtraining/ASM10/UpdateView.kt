package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM10

interface UpdateView {
    fun updateViewChia3(number:Int)
    fun updateViewChia5(number:Int)
    fun updateViewSoChan(number:Int)
    fun updateViewSoLe(number:Int)
    fun updateViewNguyenTo(number:Int)
}