package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM8

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.fresher2022_kotlin_hoangdm11.R

class PlayingFragment(var mMediaController: MediaController) : Fragment() {
    private var tvSongName: TextView? = null
    private var btnPlay: Button? = null
    private var btnPrev: Button? = null
    private var btnNext: Button? = null
    private var seekBar: SeekBar? = null

    private var mSong: SongInfo? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        print("PlayingFragment onCreateView call \n")
        val view = inflater.inflate(R.layout.playing_fragment, container, false)
        tvSongName = view.findViewById(R.id.tv_songNameFargment_8)
        btnPlay = view.findViewById(R.id.btn_play_8)
        btnPrev = view.findViewById(R.id.btn_pre_8)
        btnNext = view.findViewById(R.id.btn_next_8)
        seekBar = view.findViewById(R.id.seekbar_song)

        arguments?.let {
            mSong = it.getSerializable("song") as SongInfo?
            if (mSong != null) {
                tvSongName!!.text = mSong!!.title
            }
            btnPlay!!.setOnClickListener() {
                clickPlayOrPauseButton()
            }
            btnPrev!!.setOnClickListener() {
                onClickPrevSong()
            }
            btnNext!!.setOnClickListener() {
                onClickNextSong()
            }
            seekBar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                    onClickSeekBarSong()
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                    onClickSeekBarSong()
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                    onClickSeekBarSong()
                }

            })
        }
        return view
    }

    private fun clickPlayOrPauseButton() {
        mMediaController.onClickPlayOrPause()
    }

    private fun onClickPrevSong() {
        mMediaController.onClickPrevSong()
    }

    private fun onClickNextSong() {
        mMediaController.onClickNextSong()
    }

    private fun onClickSeekBarSong() {
        mMediaController.onClickSeekBar()
    }
}