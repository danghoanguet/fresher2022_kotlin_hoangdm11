package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM23.bai1

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.fresher2022_kotlin_hoangdm11.R


class ActivityA : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a)

        val bundle = Bundle()
        val myList = arrayListOf<String>(
            "Hello!",
            "Hi!",
            "Salut!",
            "Hallo!",
            "Ciao!",
            "Ahoj!",
            "YAHsahs!",
            "Bog!",
            "Hej ! ",
            " Czesc!",
            " Ní! ",
            " Kon'nichiwa! ",
            " Annyeonghaseyo!",
            "Shalom!",
            "Sah-wahd-dee-kah! ",
            " Merhaba!",
            " Hujambo! ",
            " Olá! "
        )
        bundle.putStringArrayList("myList",
        myList
        )
        val intent = Intent(this, ActivityB::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }
}