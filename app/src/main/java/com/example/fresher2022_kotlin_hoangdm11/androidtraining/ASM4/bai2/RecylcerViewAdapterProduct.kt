package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.fresher2022_kotlin_hoangdm11.R


class RecylcerViewAdapterProduct(
    var listProduct: ArrayList<Product>
) : RecyclerView.Adapter<RecylcerViewAdapterProduct.ProductViewHolder>() {

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener) {
        mListener = listener
    }

   inner class ProductViewHolder(itemView:View, listener: onItemClickListener): RecyclerView.ViewHolder(itemView) {
        var tvId: TextView? = null
        var tvName: TextView? = null
        var tvPrice: TextView? = null
        var tvDescribe: TextView? = null
        var tvImageView: ImageView? = null

       init {
           tvId = itemView.findViewById(R.id.idproduct) as TextView
           tvName = itemView.findViewById(R.id.nameproduct) as TextView
           tvPrice = itemView.findViewById(R.id.priceproduct) as TextView
           tvDescribe = itemView.findViewById(R.id.describeproduct) as TextView
           tvImageView = itemView.findViewById(R.id.image_view_product) as ImageView

           itemView.setOnClickListener{
               listener.onItemClick(adapterPosition)
           }
       }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val productView: View = inflater.inflate(R.layout.product_view,parent,false)
        return ProductViewHolder(productView,mListener)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product : Product = listProduct[position]
        holder.tvId!!.text = String.format("ID = %d", product.productID)
        holder.tvName!!.text = String.format("Tên SP : %s", product.name)
        holder.tvPrice!!.text = String.format("Giá %f", product.price)
        holder.tvDescribe!!.text = String.format("Mô tả %s", product.describe)
        // viewHolder.tvImageView!!.setImageURI(Uri.parse(product.imageURL))
        holder.tvImageView!!.setImageResource(R.drawable.image_1)
    }

    fun getItem(position: Int): Any? {
        //Cần trả về đối tượng dữ liệu phần tử ở vị trí position
        return listProduct[position]
    }
    override fun getItemCount(): Int {
        return listProduct.size
    }
}

