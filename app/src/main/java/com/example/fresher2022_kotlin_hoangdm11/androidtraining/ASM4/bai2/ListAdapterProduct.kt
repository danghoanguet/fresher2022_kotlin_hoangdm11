package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.fresher2022_kotlin_hoangdm11.R


class ListAdapterProduct(
    var listProduct: ArrayList<Product>
) : BaseAdapter() {
    override fun getCount(): Int {
        //Cần trả về số phần tử mà ListView hiện thị
        return listProduct.size
    }

    override fun getItem(position: Int): Any? {
        //Cần trả về đối tượng dữ liệu phần tử ở vị trí position
        return listProduct[position]
    }

    override fun getItemId(position: Int): Long {
        //Trả về một ID liên quan đến phần tử ở vị trí position
        return listProduct[position].productID.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        //convertView là View hiện thị phần tử, nếu là null cần tạo mới
        //(có thể nạp từ layout bằng View.inflate)

        //Cuối cùng là gán dữ liệu ở vị trí possition vào View và trả về đối
        //tượng View này
        var convertView = convertView
        val product = getItem(position) as Product
        val viewHolder: ViewHolder
        if (convertView == null) {
            convertView = LayoutInflater.from(parent!!.context).inflate(
                R.layout.product_view, parent,
                false
            )
            viewHolder = ViewHolder()
            viewHolder.tvId = convertView.findViewById(R.id.idproduct) as TextView
            viewHolder.tvName = convertView.findViewById(R.id.nameproduct) as TextView
            viewHolder.tvPrice = convertView.findViewById(R.id.priceproduct) as TextView
            viewHolder.tvDescribe = convertView.findViewById(R.id.describeproduct) as TextView
            viewHolder.tvImageView = convertView.findViewById(R.id.image_view_product) as ImageView

            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
        }
            viewHolder.tvId!!.text = String.format("ID = %d", product.productID)
            viewHolder.tvName!!.text = String.format("Tên SP : %s", product.name)
            viewHolder.tvPrice!!.text = String.format("Giá %f", product.price)
            viewHolder.tvDescribe!!.text = String.format("Mô tả %s", product.describe)
        //TODO: convert String imageURL to ImageView resource
           // viewHolder.tvImageView!!.setImageURI(Uri.parse(product.imageURL))
            viewHolder.tvImageView!!.setImageResource(R.drawable.image_1)


        return convertView
    }
}

internal class ViewHolder {
    var tvId: TextView? = null
    var tvName: TextView? = null
    var tvPrice: TextView? = null
    var tvDescribe: TextView? = null
    var tvImageView: ImageView? = null
}