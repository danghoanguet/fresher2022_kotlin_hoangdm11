package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM6.Bai2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.preference.PreferenceManager
import com.example.fresher2022_kotlin_hoangdm11.R
import java.io.Serializable

class SettingActivity : AppCompatActivity() {
    private val fragmentManager = supportFragmentManager
    private val fragmentTransaction = fragmentManager.beginTransaction()
    private val settingsFragmet = PreferenceFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        fragmentTransaction.add(R.id.frame_layout_setting,settingsFragmet)
        fragmentTransaction.commit()

        findViewById<Button>(R.id.btn_6_save).setOnClickListener() {
            val prefs = PreferenceManager.getDefaultSharedPreferences(this)
            val checkbox = prefs.getBoolean("checkbox",false)
            val editText = prefs.getString("text","")
            val list = prefs.getString("list","")
            val switch = prefs.getBoolean("switch",false)
            val multiSelectList = prefs.getStringSet("multiSelectList", HashSet<String>())
            val seekBar = prefs.getInt("seekbar",0)
            Log.d("Prefs","checkbox: ${checkbox}\nEdit Text: $editText\nList: $list\nSwitch: $switch\nMultiList: $multiSelectList\nSeek Bar: $seekBar\n")
            val intent = Intent()
            val bundle = Bundle()
            bundle.putString("checkbox", checkbox.toString())
            bundle.putString("text", editText)
            bundle.putString("list", list)
            bundle.putString("switch", switch.toString())
            bundle.putSerializable("multiSelectList",multiSelectList as Serializable)
            bundle.putString("seekbar", seekBar.toString())
            intent.putExtras(bundle)
            setResult(1,intent)
            finish()
        }
    }

}