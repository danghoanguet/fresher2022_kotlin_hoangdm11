package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM10

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import com.example.fresher2022_kotlin_hoangdm11.R
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class Activity10 : AppCompatActivity(), UpdateView, OnFinish {
    val mySubject: MySubject = MySubject()

    var fragmentManager = supportFragmentManager
    var fragmentTransaction = fragmentManager.beginTransaction()

    val fragmentChia3Observer: FragmentChia3Observer = FragmentChia3Observer(this)
    val fragmentChia3 = FragmentChia3(fragmentChia3Observer, this)

    val fragmentChia5Observer: FragmentChia5Observer = FragmentChia5Observer(this)
    val fragmentChia5 = FragmentChia5(fragmentChia5Observer, this)

    val fragmentSoChanObserver: FragmentSoChanObserver = FragmentSoChanObserver(this)
    val fragmentSoChan = FragmentSoChan(fragmentSoChanObserver, this)

    val fragmentSoLeObserver: FragmentSoLeObserver = FragmentSoLeObserver(this)
    val fragmentSoLe = FragmentSoLe(fragmentSoLeObserver, this)

    val fragmentNguyenToObserver: FragmentNguyenToObserver = FragmentNguyenToObserver(this)
    val fragmentNguyenTo = FragmentNguyenTo(fragmentNguyenToObserver, this)

    var maxNumber: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_10)

        mySubject.registerObserver(fragmentChia3Observer)
        mySubject.registerObserver(fragmentChia5Observer)
        mySubject.registerObserver(fragmentSoChanObserver)
        mySubject.registerObserver(fragmentSoLeObserver)
        mySubject.registerObserver(fragmentNguyenToObserver)


        fragmentTransaction.add(R.id.frame_layout_chia3, fragmentChia3)
        fragmentTransaction.add(R.id.frame_layout_chia5, fragmentChia5)
        fragmentTransaction.add(R.id.frame_layout_sochan, fragmentSoChan)
        fragmentTransaction.add(R.id.frame_layout_sole, fragmentSoLe)
        fragmentTransaction.add(R.id.frame_layout_nguyento, fragmentNguyenTo)
        fragmentTransaction.commit()

        findViewById<Button>(R.id.btn_play_or_stop).setOnClickListener() {
            val input = findViewById<EditText>(R.id.et_max).text.toString()
            print("is playing0: ${mySubject.isPlaying}\n")
            mySubject.isPlaying = !mySubject.isPlaying
            print("is playing1: ${mySubject.isPlaying}\n")
            if (!mySubject.isPlaying) {
                maxNumber = 0
                findViewById<EditText>(R.id.et_max).text = 0.toString().toEditable()
                mySubject.playOrStop(maxNumber)
            } else {
                maxNumber = findViewById<EditText>(R.id.et_max).text.toString().toInt()
                mySubject.playOrStop(maxNumber)
            }
        }
        findViewById<ImageButton>(R.id.btn_add).setOnClickListener() {
            val input = findViewById<EditText>(R.id.et_max).text.toString()
            maxNumber = input.toInt() + 1
            findViewById<EditText>(R.id.et_max).text = maxNumber.toString().toEditable()
            mySubject.isPlaying = true
            mySubject.playOrStop(maxNumber)
        }
        findViewById<ImageButton>(R.id.btn_miunus).setOnClickListener() {
            val input = findViewById<EditText>(R.id.et_max).text.toString()
            maxNumber = input.toInt()
            if (maxNumber > 0) {
                maxNumber -= 1
                findViewById<EditText>(R.id.et_max).text = maxNumber.toString().toEditable()
                mySubject.isPlaying = true
                mySubject.playOrStop(maxNumber)
            } else {
                mySubject.playOrStop(-1)
            }
        }
    }

    override fun updateViewChia3(number: Int) {
        val fragmentChia3: FragmentChia3 = FragmentChia3(fragmentChia3Observer, this)
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout_chia3, fragmentChia3)
        fragmentTransaction.commit()
    }

    override fun updateViewChia5(number: Int) {
        val fragmentChia5: FragmentChia5 = FragmentChia5(fragmentChia5Observer, this)
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout_chia5, fragmentChia5)
        fragmentTransaction.commit()
    }

    override fun updateViewSoChan(number: Int) {
        val fragmentSoChan: FragmentSoChan = FragmentSoChan(fragmentSoChanObserver, this)
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout_sochan, fragmentSoChan)
        fragmentTransaction.commit()
    }

    override fun updateViewSoLe(number: Int) {
        val fragmentSoLe: FragmentSoLe = FragmentSoLe(fragmentSoLeObserver, this)
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout_sole, fragmentSoLe)
        fragmentTransaction.commit()
    }

    override fun updateViewNguyenTo(number: Int) {
        val fragmentNguyenTo: FragmentNguyenTo = FragmentNguyenTo(fragmentNguyenToObserver, this)
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout_nguyento, fragmentNguyenTo)
        fragmentTransaction.commit()
    }

    override fun onFinish() {
        mySubject.isPlaying = false
        //findViewById<EditText>(R.id.et_max).text = 0.toString().toEditable()
        print("is playing3: ${mySubject.isPlaying}\n")
    }

    fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)
}
