package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM23.bai4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.example.fresher2022_kotlin_hoangdm11.R

class ActivityB4() : AppCompatActivity() {
    val ACTION_SEND = ".androidtraining.bai4.ACTION_SEND"
    val KEY_NUM = "NUMBER"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b4)
        title = "ActivityB4"
        val btn = findViewById<Button>(R.id.buttonB4)
        btn.setOnClickListener{
            onClick()
        }
    }
    private fun onClick() {
        val intent = Intent(ACTION_SEND)
        val bundle = Bundle()
        val number = (1..5).random()
        Log.i("", "$number")
        bundle.putInt(KEY_NUM, number)
        intent.putExtras(bundle)
        sendOrderedBroadcast(intent, null)
        Log.i("ActivityB4", "Sent Broadcast. Data: $number")
    }

}