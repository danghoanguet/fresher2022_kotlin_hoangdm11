package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM8

interface MediaController {
    fun onClickPlayOrPause()
    fun onClickPrevSong()
    fun onClickNextSong()
    fun onClickSeekBar()
}