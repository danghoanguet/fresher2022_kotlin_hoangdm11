package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM6.Bai2

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.preference.PreferenceManager
import com.example.fresher2022_kotlin_hoangdm11.R

class Activity6_Task2 : AppCompatActivity() {
    var tv_1: TextView? = null
    var tv_2 : TextView? = null
    var tv_3 : TextView? = null
    var tv_4: TextView? = null
    var tv_5 : TextView? = null
    var tv_6 : TextView? = null
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity6_task2)

         tv_1 = findViewById<TextView>(R.id.tv_6_1)
         tv_2 = findViewById<TextView>(R.id.tv_6_2)
         tv_3 = findViewById<TextView>(R.id.tv_6_3)
         tv_4 = findViewById<TextView>(R.id.tv_6_4)
         tv_5 = findViewById<TextView>(R.id.tv_6_5)
         tv_6 = findViewById<TextView>(R.id.tv_6_6)

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val checkbox = prefs.getBoolean("checkbox",false)
        val editText = prefs.getString("text","")
        val list = prefs.getString("list","")
        val switch = prefs.getBoolean("switch",false)
        val multiSelectList = prefs.getStringSet("multiSelectList", HashSet<String>())
        val seekBar = prefs.getInt("seekbar",0)

        tv_1?.text = "CheckBoxPreference: " + checkbox
        tv_2?.text = "EditTextPreference: " + editText
        tv_3?.text = "ListPreference: " + list
        tv_4?.text = "SwitchPreference: " + switch
        tv_5?.text = "MultiSelectListPreference: " + multiSelectList
        tv_6?.text = "SeekBarPreference: " + seekBar
        val btn = findViewById<Button>(R.id.btn_6_setting).setOnClickListener() {
            startActivityForResult(Intent(this,SettingActivity::class.java),1)
        }

    }



    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == 1) {
            if( resultCode == 1) {
                if( data != null) {
                    val myData = data.extras
                    tv_1!!.text = "CheckBoxPreference: " + myData!!.getString("checkbox")
                    tv_2!!.text = "EditTextPreference: " + myData.getString("text")
                    tv_3!!.text = "ListPreference: " + myData.getString("list")
                    tv_4!!.text = "SwitchPreference: " + myData.getString("switch")
                    tv_5!!.text = "MultiSelectListPreference: " + myData.getSerializable("multiSelectList")
                    tv_6!!.text = "SeekBarPreference: " + myData.getString("seekbar")
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}