package com.example.fresher2022_kotlin_hoangdm11.androidtraining.HoangDM11_BAD_FinalTest

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM8.SongInfo

class CountingService:Service() {
    var COUNTING_SERVICE_STATUS_OK = "OK"
    var COUNTING_SERVICE_STATUS_DESTROY = "DESTROY"

    var sum:Long = 0
    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        object : Thread() {
            override fun run() {
                for(i in 0..1000000) {
                    sum += i
                }
                sleep(5000)
                sendResponseToActivity(COUNTING_SERVICE_STATUS_OK)
                stopSelf()
                super.run()
            }

        }.start()

        return super.onStartCommand(intent, flags, startId)
    }
    private fun sendResponseToActivity(status:String) {
        var intent = Intent("send_response_to_activity")
        var bundle = Bundle()
        bundle.putString("status",status)
        bundle.putLong("sum", sum)
        intent.putExtras(bundle)

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onDestroy() {
        sendResponseToActivity(COUNTING_SERVICE_STATUS_DESTROY)
        super.onDestroy()
    }

}