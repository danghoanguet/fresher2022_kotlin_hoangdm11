package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai1.ConstraintLayoutDemo.LinearLayoutDemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.example.fresher2022_kotlin_hoangdm11.R

class LL_ActivityB : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ll_b)
        title = "LL_ActivityB"
        val data = this.intent.extras

        findViewById<TextView>(R.id.id_text_view_welcom).text = ("Chào mừng " + data!!.getString("HO_TEN")) ?: ""
        findViewById<TextView>(R.id.id_text_view_account).text = ("Tài khoản: " + data.getString("TAI_KHOAN"))?: ""
        findViewById<TextView>(R.id.id_text_view_age).text = ("Tuổi: "  + data.getString("TUOI"))?:""
        findViewById<TextView>(R.id.id_text_view_gender).text = ("Giới tính: "+ data.getString("GIOI_TINH"))?:""

        val btnOK = findViewById<Button>(R.id.id1_button_OK).setOnClickListener() {
            setResult(1)
            finish()
        }
        val btnCancel = findViewById<Button>(R.id.id1_button_Cancel).setOnClickListener() {
            setResult(2)
            finish()
        }

        Log.i("","Tai khoan: ${data?.getString("TAI_KHOAN")}\nHo ten: ${data?.getString("HO_TEN")}\nTuoi: ${data?.getString("TUOI")}\nGioi tinh: ${data?.getString("GIOI_TINH")}\n")

    }
}