package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM9

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fresher2022_kotlin_hoangdm11.R
import org.w3c.dom.Text

class Activity9B : AppCompatActivity() {
    var listContact = arrayListOf<Contact>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity9_b)

        checkPermission()

        if(listContact.isEmpty()) {
            findViewById<RecyclerView>(R.id.contact_list).visibility = View.GONE
            findViewById<TextView>(R.id.tv_empty).visibility = View.VISIBLE
        }else {
            val listContactRecyclerViewAdapter = ListContactAdapter(listContact)
            val linearLayoutManager = LinearLayoutManager(this)
            val listContactRecyclerView = findViewById<RecyclerView>(R.id.contact_list)
            listContactRecyclerView.adapter = listContactRecyclerViewAdapter
            listContactRecyclerView.layoutManager = linearLayoutManager
        }
    }

    private fun checkPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED) {
            requestContactPermission()
        }
        else {
            getContactList()
        }
    }

    @SuppressLint("Range")
    private fun getContactList() {
        val uri: Uri = ContactsContract.Contacts.CONTENT_URI
        val sort = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME

        val cursor: Cursor? = contentResolver.query(uri,null,null,null,sort)

        if(cursor!!.count > 0) {
            while (cursor.moveToNext()) {
                val id: String = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name: String = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))

                val uriPhone: Uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI

                val selection:String = ContactsContract.CommonDataKinds.Phone.CONTACT_ID

                val phoneCursor: Cursor? = contentResolver.query(uriPhone,null,selection, null,null)

                if(phoneCursor!!.moveToNext()) {
                    val number = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    val contact = Contact(name,number)
                    listContact.add(contact)
                    phoneCursor.close()
                }
            }
            cursor.close()
        }

    }

    private fun requestContactPermission() {
        val permission: Array<String> = arrayOf(android.Manifest.permission.READ_CONTACTS)
        ActivityCompat.requestPermissions(this,permission,0)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == 0 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getContactList()
        }else {
            Toast.makeText(this,"Permission denied", Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}