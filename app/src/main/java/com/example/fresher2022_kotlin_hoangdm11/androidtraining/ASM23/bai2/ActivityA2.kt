package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM23.bai2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.fresher2022_kotlin_hoangdm11.R

class ActivityA2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a2)
        Log.i("info", "Im on ActivityA2")

        val btn = findViewById<Button>(R.id.button)
        btn.setOnClickListener{
            val firstNumber = findViewById<EditText>(R.id.id1_edit_text).text
            val secondNumber = findViewById<EditText>(R.id.id2_edit_text).text
            val bundle = Bundle()
            bundle.putString("first_number", firstNumber.toString())
            bundle.putString("second_number",secondNumber.toString())
            val intent = Intent(this, ActivityB2::class.java)
            intent.putExtras(bundle)
            startActivityForResult(intent,2)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 2) {
            if (resultCode == 3) {
                if (data != null) {
                    val res = data.getIntExtra("sum",0)
                    Log.i("Sum return from ActivityB2: ","$res")
                    Toast.makeText(this, "Sum is: ${res}", Toast.LENGTH_SHORT).show()
                    //Clear Edit Text
                    findViewById<EditText>(R.id.id1_edit_text).setText("")
                    findViewById<EditText>(R.id.id2_edit_text).setText("")
                }
                else {
                    print("Data null")
                }
            }
            else {
                print("resultCode mismatch")
            }
        } else {
            print("requestCode mismatch")
        }
    }
}