package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM6.Bai1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import com.example.fresher2022_kotlin_hoangdm11.R

class Activity6 : AppCompatActivity(), MyDialogInterface {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_6)

        val fragmentManager: FragmentManager = supportFragmentManager
        val myDialogFragment: MyDialogFragment = MyDialogFragment(this)
        findViewById<Button>(R.id.btn_6).setOnClickListener() {
            myDialogFragment.show(fragmentManager,null)
        }
    }

    override fun sendData(msg: String) {
        findViewById<TextView>(R.id.tv_6).text = msg
        super.sendData(msg)
    }
}

