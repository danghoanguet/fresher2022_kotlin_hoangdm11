package com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM7

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.example.fresher2022_kotlin_hoangdm11.R
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM7.FormFragment
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM5.FragmentTransactionInterface
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM5.RecyclerViewFragment

class Activity7 : AppCompatActivity() {

    val fragmentManager = supportFragmentManager
    val fragmentTransaction = fragmentManager.beginTransaction()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_7)

        val navHostFragment = NavHostFragment.create(R.navigation.nav_graph_demo)
        fragmentTransaction.replace(R.id.frame_layout_full,navHostFragment).setPrimaryNavigationFragment(navHostFragment).commit()
    }

}