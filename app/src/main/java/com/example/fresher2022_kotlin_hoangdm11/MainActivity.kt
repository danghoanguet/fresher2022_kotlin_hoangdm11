package com.example.fresher2022_kotlin_hoangdm11

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM10.Activity10
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM4.bai2.ASM4_ActivityA
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM6.Bai1.Activity6
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM6.Bai2.Activity6_Task2
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM7.Activity7
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM8.Activity8
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.ASM9.Activity9
import com.example.fresher2022_kotlin_hoangdm11.androidtraining.HoangDM11_BAD_FinalTest.MainActivity11

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Bai1
//        intent = Intent(this, ActivityA::class.java)
//        startActivity(intent)

        //Bai2
//        intent = Intent(this, ActivityA2::class.java)
//        startActivity(intent)

        //Bai3
        //intent = Intent(this, ActivityA3::class.java)
        //startActivity(intent)
        //Bai4
        //startActivity(Intent(this, ActivityB4::class.java))

        //ASM4
        //Bai1
        //startActivity(Intent(this, CL_ActivityA::class.java))
        //Bai2
        //startActivity(Intent(this, ASM4_ActivityA::class.java))

        //ASM5
        //startActivity(Intent(this, ActivityASM5::class.java))

        //ASM6
        //Bai1
        //startActivity(Intent(this, Activity6::class.java))
        //Bai2
        //startActivity(Intent(this,Activity6_Task2::class.java))

        //ASM7
        //startActivity(Intent(this, Activity7::class.java))

        //ASM8
        //startActivity(Intent(this, Activity8::class.java))

        //ASM9
        //startActivity(Intent(this, Activity9::class.java))

        //ASM10
        //startActivity(Intent(this, Activity10::class.java))

        //Basic android final
        startActivity(Intent(this, MainActivity11::class.java))
    }
}